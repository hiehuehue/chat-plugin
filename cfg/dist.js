'use strict';

let path = require('path');
let webpack = require('webpack');

let baseConfig = require('./base');
let defaultSettings = require('./defaults');
// Add needed plugins here
let BowerWebpackPlugin = require('bower-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const argv = require('minimist')(process.argv.slice(2));
const server = argv.server ? argv.server === 'production' ? argv.server : argv.server === 'alpha' ? argv.server : argv.server === 'beta' ? argv.server : 'staging' : 'staging';
let globals = require('./globals')(server);
console.log(Object.assign({}, {
  'process.env.NODE_ENV': '"production"'
}, globals));
let config = Object.assign({}, baseConfig, {
  entry: path.join(__dirname, '../src/index'),
  cache: true,
  devtool: 'sourcemap',
  plugins: [
    new webpack.optimize.DedupePlugin(),
    new webpack.DefinePlugin(Object.assign({}, {
      'process.env.NODE_ENV': '"production"'
    }, globals)),
    new BowerWebpackPlugin({
      searchResolveModulesDirectories: false
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        drop_console: true
      }
    }),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.AggressiveMergingPlugin(),
    new webpack.NoErrorsPlugin(),
    new ExtractTextPlugin('styles.css')
  ],
  module: defaultSettings.getDefaultModules()
});

// Add needed loaders to the defaults here
config.module.loaders.push({
  test: /\.(js|jsx)$/,
  loader: 'babel',
  include: [].concat(
    config.additionalPaths, [path.join(__dirname, '/../src')]
  )
});
config.module.loaders.push({
  test: /\.scss$/,
  loader: ExtractTextPlugin.extract('style-loader', 'css-loader?modules&localIdentName=PSTC_[local]_[hash:base64:5]!sass-loader')
});

module.exports = config;
