const ENV_VARIABLE = require('./env');
module.exports = function globals(server) {
  return {
    'PQUERY_ASSETS': server === 'staging' ?
      JSON.stringify('') : server === 'alpha' ?
      JSON.stringify('//alpha.pstakecare.com/pquery/') : server === 'beta' ? JSON.stringify('//beta.pstakecare.com/pquery/') : server === 'production' ? JSON.stringify('//pstakecare.com/pquery/') : JSON.stringify(''),
    'CORS_API_STAGING': server === 'staging' ?
      JSON.stringify('http://staging.pstakecare.com') : server === 'alpha' ?
      JSON.stringify('https://alpha.pstakecare.com') : server === 'beta' ?
      JSON.stringify('https://beta1.pstakecare.com') : server === 'production' ?
      JSON.stringify('https://pstakecare.com') : JSON.stringify('http://staging.pstakecare.com'),
    'CORS_API_APPLENEST': server === 'staging' ?
      JSON.stringify('http://chat.applenest.com') : server === 'alpha' ?
      JSON.stringify('https://chat.alpha.pstakecare.com') : server === 'beta' ?
      JSON.stringify('https://chat.beta.pstakecare.com') : server === 'production' ? JSON.stringify('https://chat.pstakecare.com') : JSON.stringify('http://chat.applenest.com'),
    'DOMAIN': server === 'staging' ?
      JSON.stringify('chat.applenest.com') : server === 'alpha' ?
      JSON.stringify('chat.alpha.pstakecare.com') : server === 'beta' ?
      JSON.stringify('chat.beta.pstakecare.com') : server === 'production' ?
      JSON.stringify('chat.pstakecare.com') : JSON.stringify('chat.applenest.com'),
    'HOST': server === 'staging' ?
      JSON.stringify('ws://chat.applenest.com:5280/websocket') : server === 'alpha' ?
      JSON.stringify('wss://chat.alpha.pstakecare.com:5284/websocket') : server === 'beta' ?
      JSON.stringify('wss://chat.beta.pstakecare.com:5284/websocket') : server === 'production' ?
      JSON.stringify('wss://chat.pstakecare.com:5284/websocket') : JSON.stringify('ws://chat.applenest.com:5280/websocket'),
    'RAZORPAY_KEY': server === 'beta' || server === 'production' ?
      JSON.stringify(ENV_VARIABLE.dist.RAZORPAY_KEY) : JSON.stringify(ENV_VARIABLE.dev.RAZORPAY_KEY),
    'DEFAULT_GA_ID': server === 'staging' ?
      JSON.stringify('UA-98048452-1') : server === 'alpha' ?
      JSON.stringify('UA-98048452-1') : server === 'beta' ?
      JSON.stringify('UA-98048452-1') : server === 'production' ?
      JSON.stringify('UA-98047663-1') : JSON.stringify('UA-98048452-1')
  }
}
