'use strict';

let path = require('path');
let webpack = require('webpack');
let baseConfig = require('./base');
let defaultSettings = require('./defaults');

// Add needed plugins here
let BowerWebpackPlugin = require('bower-webpack-plugin');
const argv = require('minimist')(process.argv.slice(2));
const server = argv.server ? argv.server === 'alpha' ? argv.server : argv.server === 'beta' ? argv.server : 'staging' : 'staging';
let globals = require('./globals')(server);
let config = Object.assign({}, baseConfig, {
  entry: [
    'webpack-dev-server/client?http://127.0.0.1:' + defaultSettings.port,
    'webpack/hot/only-dev-server',
    './src/index'
  ],
  cache: true,
  devtool: 'cheap-module-source-map',
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new BowerWebpackPlugin({
      searchResolveModulesDirectories: false
    }),
    new webpack.DefinePlugin(Object.assign({}, {
      'process.env.NODE_ENV': '"development"'
    }, globals))
  ],
  module: defaultSettings.getDefaultModules()
});

// Add needed loaders to the defaults here
config.module.loaders.push({
  test: /\.scss$/,
  loader: 'style-loader!css-loader?modules&localIdentName=PSTC_[local]_[hash:base64:5]!sass-loader?outputStyle=expanded',
  include: [].concat(
    config.additionalPaths, [path.join(__dirname, '/../src')]
  )
});
config.module.loaders.push({
  test: /\.(js|jsx)$/,
  loader: 'react-hot!babel-loader',
  include: [].concat(
    config.additionalPaths, [path.join(__dirname, '/../src')]
  )
});


module.exports = config;
