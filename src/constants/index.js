export const CHANGE_VISIBILITY_OF_CHAT = 'CHANGE_VISIBILITY_OF_CHAT';
export const CHANGE_SCREEN_STATE = 'CHANGE_SCREEN_STATE';
export const CHANGE_NAME = 'CHANGE_NAME';
export const CHANGE_PHONENO = 'CHANGE_PHONENO';
export const CHANGE_QUERY_BODY = 'CHANGE_QUERY_BODY';
export const SUBMIT_FIRST_SCREEN_FORM = 'SUBMIT_FIRST_SCREEN_FORM';
export const CHANGE_SUBMIT_ABILITY = 'CHANGE_SUBMIT_ABILITY';
export const CHANGE_SCREEN = 'CHANGE_SCREEN';
export const CHANGE_OTP_VALUE = 'CHANGE_OTP_VALUE';
export const CONFIRM_OTP = 'CONFIRM_OTP';
export const RESEND_OTP = 'RESEND_OTP';
export const SET_USER_VERIFICATION_STATUS = 'SET_USER_VERIFICATION_STATUS';
export const SAVE_AUTHENTICATION_TOKEN = 'SAVE_AUTHENTICATION_TOKEN';
export const CHANGE_OTP_RESEND_ABILITY = 'CHANGE_OTP_RESEND_ABILITY';
export const CHANGE_OTP_SUBMIT_ABILITY = 'CHANGE_OTP_SUBMIT_ABILITY';
export const SAVE_CHAT_PASSWORD = 'SAVE_CHAT_PASSWORD';
export const SAVE_JID = 'SAVE_JID';
export const SAVE_JID_AND_PASSWORD = 'SAVE_JID_AND_PASSWORD';
export const INIT_XMPP_CONNNECTION = 'INIT_XMPP_CONNNECTION';
export const STORE_MESSAGES = 'STORE_MESSAGES';
export const WATCH_ON_MESSAGE = 'WATCH_ON_MESSAGE';
export const CHANGE_MESSAGE_VALUE = 'CHANGE_MESSAGE_VALUE';
export const CHANGE_RECEIVER_JID_AND_NAME = 'CHANGE_RECEIVER_JID_AND_NAME';
export const CHANGE_QUERY_ID = 'CHANGE_QUERY_ID';
export const SEND_AND_STORE_MESSAGE = 'SEND_AND_STORE_MESSAGE';
export const OPEN_NEW_QUERY = 'OPEN_NEW_QUERY';
export const CLOSE_XMPP_CONNECTION = 'CLOSE_XMPP_CONNECTION';
export const UNWATCH_ON_MESSAGES = 'UNWATCH_ON_MESSAGES';
export const SUBMIT_QUERY_AGAIN = 'SUBMIT_QUERY_AGAIN';
export const SAVE_DOCTOR_PROFILE = 'SAVE_DOCTOR_PROFILE';
export const SHOW_PAYMENT_SCREEN = 'SHOW_PAYMENT_SCREEN';
export const SUBMIT_PAYMENT_DETAILS = 'SUBMIT_PAYMENT_DETAILS';
export const SAVE_PAYMENT_DETAILS = 'SAVE_PAYMENT_DETAILS';
export const SAVE_DOCTOR_PROFILE_ID = 'SAVE_DOCTOR_PROFILE_ID';
export const POP_UP_SCREEN = 'POP_UP_SCREEN';
export const CALL_DOCTOR_PROFILE = 'CALL_DOCTOR_PROFILE';
export const DO_DOUBLE_TICK = 'DO_DOUBLE_TICK';
export const DO_SINGLE_TICK = 'DO_SINGLE_TICK';
export const WATCH_ON_RECIPIENT_ACK = 'WATCH_ON_RECIPIENT_ACK';
export const WATCH_ON_SERVER_ACK = 'WATCH_ON_SERVER_ACK';
export const UPLOAD_AND_SEND_FILE = 'UPLOAD_AND_SEND_FILE';
export const CHANGE_PROMOCODE = 'CHANGE_PROMOCODE';
export const GET_PROMOCODE_DETAILS = 'GET_PROMOCODE_DETAILS';
export const SAVE_PROMOCODE_DETAILS = 'SAVE_PROMOCODE_DETAILS';
export const GET_QUERY_PAYMENT_DETAILS = 'GET_QUERY_PAYMENT_DETAILS';
export const CLOSE_ERROR_SCREEN = 'CLOSE_ERROR_SCREEN';
export const SAVE_QUERY_PAYMENT_DETAILS = 'SAVE_QUERY_PAYMENT_DETAILS';
export const PUSH_ERROR = 'PUSH_ERROR';
export const STORE_JID_AND_NAME_FOR_DOCTOR = 'STORE_JID_AND_NAME_FOR_DOCTOR';
export const CHANGE_RATING = 'CHANGE_RATING';
export const SUBMIT_RATING = 'SUBMIT_RATING';
export const START_OTP_TIMER = 'START_OTP_TIMER';
export const CHANGE_RESEND_TICK = 'CHANGE_RESEND_TICK';
export const CHANGE_RESEND_BUTTON_VISIBILITY = 'CHANGE_RESEND_BUTTON_VISIBILITY';
export const CONTINUE_24_HOUR_TIMER = 'CONTINUE_24_HOUR_TIMER';
export const CHANGE_24_HOUR_TIMER_VISIBILITY = 'CHANGE_24_HOUR_TIMER_VISIBILITY';
export const CHANGE_24_HOUR_TIME = 'CHANGE_24_HOUR_TIME';
export const SAVE_PAYMENT_DONE_TIME = 'SAVE_PAYMENT_DONE_TIME';
export const START_24_HOUR_TIMER = 'START_24_HOUR_TIMER';
export const CHANGE_BOTTOM_ACTION_BUTTON = 'CHANGE_BOTTOM_ACTION_BUTTON';
export const CHANGE_NUMBER = 'CHANGE_NUMBER';
export const CHANGE_ERROR_MESSAGE_FOR_PHONE_NO = 'CHANGE_ERROR_MESSAGE_FOR_PHONE_NO';
export const CHANGE_ERROR_MESSAGE_FOR_NAME = 'CHANGE_ERROR_MESSAGE_FOR_NAME';
export const STOP_24_HOUR_TIMER = 'STOP_24_HOUR_TIMER';
export const INCREASE_UNREAD_MESSAGE_COUNT = 'INCREASE_UNREAD_MESSAGE_COUNT';
export const CLEAR_UNREAD_MESSAGE_COUNT = 'CLEAR_UNREAD_MESSAGE_COUNT';
export const CHANGE_QUERY_RE_SUBMIT_ABILITY = 'CHANGE_QUERY_RE_SUBMIT_ABILITY';
export const CHANGE_OTP_ERROR_MESSAGE = 'CHANGE_OTP_ERROR_MESSAGE';
export const CHANGE_ERROR_FOR_NEW_QUERY_BODY = 'CHANGE_ERROR_FOR_NEW_QUERY_BODY';
export const CHANGE_TITLE = 'CHANGE_TITLE';
export const PAYMENT_REQUESTED = 'PAYMENT_REQUESTED';
export const CHANGE_PAYMENT_COMPLETE_STATUS = 'CHANGE_PAYMENT_COMPLETE_STATUS';
export const SYNC_CHAT_HISTORY = 'SYNC_CHAT_HISTORY';
export const AMMEND_STORED_MESSAGE = 'AMMEND_STORED_MESSAGE';
export const STORE_JID_AND_NAME_FOR_SUPPORT = 'STORE_JID_AND_NAME_FOR_SUPPORT';
export const SEND_LEFT_MESSAGES = 'SEND_LEFT_MESSAGES';
export const WATCH_ON_RECONNECTION = 'WATCH_ON_RECONNECTION';
export const CHANGE_LOADING_SCREEN_VISIBILITY = 'CHANGE_LOADING_SCREEN_VISIBILITY';
export const CHANGE_PAYMENT_SUBMIT_ABILITY = 'CHANGE_PAYMENT_SUBMIT_ABILITY';
export const SAVE_USER_EMAIL = 'SAVE_USER_EMAIL';
export const CHANGE_FIRST_USER = 'CHANGE_FIRST_USER';
export const SEND_GA = 'SEND_GA';
export const CHANGE_TO_INITIATED = 'CHANGE_TO_INITIATED';
//bottom_action_button
export const FIRST_SCREEN_NEXT = 'FIRST_SCREEN_NEXT';
export const SUBMIT_OTP_AND_QUERY = 'SUBMIT_OTP_AND_QUERY';
export const SEND_CHAT_MESSAGE = 'SEND_CHAT_MESSAGE';
export const CONTINUE_TO_PAYMENT = 'CONTINUE_TO_PAYMENT';
export const POST_NEW_QUERY = 'POST_NEW_QUERY';
export const SUMBIT_NEW_QUERY = 'SUMBIT_NEW_QUERY';
export const ON_RECONNECTION = 'ON_RECONNECTION';
//screens
export const OTP_VERIFICATION_SCREEN = 'OTP_VERIFICATION_SCREEN';
export const GET_QUERY_PHONE_NAME_SCREEN = 'GET_QUERY_PHONE_NAME_SCREEN';
export const CHAT_SCREEN = 'CHAT_SCREEN';
export const NEW_QUERY_SCREEN = 'NEW_QUERY_SCREEN';
export const PAYMENT_SCREEN = 'PAYMENT_SCREEN';
export const DOCTOR_PROFILE_SCREEN = 'DOCTOR_PROFILE_SCREEN';
//message_types
export const MSG = 'msg';
export const DOCUMENT = 'document';
export const IMAGE = 'image';
export const PICKED_BY_AGENT = 'picked_by_agent';
export const QUERY_END = 'query_end';
export const PICKED_BY_DOCTOR = 'picked_by_doctor';
export const PAYMENT_COMPLETED = 'payment_completed';
//constants
export const RESEND_TIME = 90;
export const HOUR_24_TIME = 86400000;
export const DEBUG = false;
export const SUPPORT_BOT_JID = 'support_agent';
export const SUPPORT_BOT_NAME = 'support';
export const SUPPORT_BOT_TITLE = 'PSTakeCare Consult';
export const INITIAL_TITLE = 'Consult a Doctor';
//warning
export const WARN_LEVEL_RED = 'WARN_LEVEL_RED';
export const WARN_LEVEL_YELLOW = 'WARN_LEVEL_YELLOW';
//error type
export const RELOAD = 'RELOAD';
export const RETRY = 'RETRY';
//variable constants
export const fullDomain = () => {
  return CORS_API_APPLENEST // eslint-disable-line no-undef
};
export const domain = () => {
  return DOMAIN // eslint-disable-line no-undef
};
export const host = () => {
  return HOST // eslint-disable-line no-undef
};
export var WEBSITE_GA_ID = '';
export const changeWebsiteGaId = function (value) {
  WEBSITE_GA_ID = value;
}
//breakpoints
export const BREAKPOINT_MOBILE = 400; // in px
//ga category
export const PSTAKECARE_WEBPLUGIN = 'PSTakeCare_WebPlugin';
//ga label
export const ALL_VISITORS = 'All visitors'
export const INITIAL_STATE_VISITORS = 'Initial state visitors'
export const FIRST_INTERACTION = 'Initial state visitors who interacted'
export const FIRST_QUERY_INITIATION = 'Query started by new users'
export const REPEAT_QUERY_SUBMIT = 'Query submitted by repeat users'
export const OTP_FAILURE = 'OTP Failed'
export const OTP_RESENT = 'OTP Resent'
export const OTP_EDIT = 'Edited number on OTP Screen'
export const FIRST_QUERY_SUBMIT = 'Query submitted by new users'
export const SECOND_QUERY_STARTED = 'Query started by repeat users'
export const DOCTOR_ASSIGNED = 'Doctor Assigned'
export const DOCTOR_PROFILE_VIEWED = 'Viewed DoctorProfile'
export const PAYMENT_PROCESS_STARTED = 'Started payment process'
export const PROMOCODE_APPLIED = 'Applied promocode successfully'
export const PROMOCODE_FAILURE = 'Promocode failed'
export const PAYMENT_API_FAILED = 'Payment api failed at backend'
export const PAYMENT_SUCCESSFUL_COMPLETION = 'Payment completed succesfully'
export const QUERY_RESOLVED_BY_DOCTOR = 'Query Resolved by Doctor'
export const QUERY_RESOLVED_BY_SALES = 'Query Resolved by Sales'
export const SATIFACTION_RATING_SUBMISSION = 'Satisfaction Rating Submitted'
export const DOCTOR_RATING_SUBMISSION = 'Doctor Rating Submitted'
//ga action
export const LOAD_PLUGIN = 'Load_plugin';
export const LOAD_INITIALSTATE = 'Load_InitialState';
export const CLICK_CONSULTWITHDOCTOR = 'Click_consultwithdoctor';
export const CLICK_NEXT = 'Click_Next';
export const CLICK_RESUBMIT = 'Click_ReSubmit';
export const OTP_FAILED = 'OTP_failed';
export const CLICK_RESEND = 'Click_Resend';
export const CLICK_EDITNUMBER = 'Click_Editnumber';
export const CLICK_SUBMIT = 'Click_Submit';
export const CLICK_POSTNEWQUERY = 'Click_PostNewQuery';
export const DOCTOR_ASSIGNMENT = 'Doctor_Assigned';
export const CLICK_VIEWDOCTORPROFILE = 'Click_ViewDoctorProfile';
export const CLICK_PAYNOW = 'Click_PayNow';
export const PROMOCODE_SUCCESSFUL = 'Promocode_Successful';
export const PROMOCODE_FAILED = 'Promocode_Failed';
export const PAYMENTAPI_FAILED = 'PaymentApi_Failed';
export const PAYMENT_SUCCESSFUL = 'Payment_Successful';
export const RESOLVED_BY_DOCTOR = 'Resolved_by_Doctor';
export const RESOLVED_BY_SALES = 'Resolved_by_Sales';
export const SUBMIT_SATISFACTIONRATING = 'Submit_SatisfactionRating';
export const SUBMIT_DOCTORRATING = 'Submit_DoctorRating';
//responders
export const DOCTOR = 'doctor';
export const SUPPORT = 'support';
