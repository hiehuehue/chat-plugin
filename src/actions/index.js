import * as CONSTANTS from 'constants';

export function changeToInitiated() {
  return {
    type: CONSTANTS.CHANGE_TO_INITIATED
  }
}

export function sendGA(eventCategory, eventAction, eventLabel, value) {
  return {
    type: CONSTANTS.SEND_GA,
    eventCategory,
    eventAction,
    eventLabel,
    value
  }
}

export function changeFirstUser() {
  return {
    type: CONSTANTS.CHANGE_FIRST_USER
  }
}

export function saveUserEmail(email) {
  return {
    type: CONSTANTS.SAVE_USER_EMAIL,
    email
  }
}

export function changeLoadingScreenVisibility(bool) {
  return {
    type: CONSTANTS.CHANGE_LOADING_SCREEN_VISIBILITY,
    bool
  }
}

export function changePaymentSubmitAbility(bool) {
  return {
    type: CONSTANTS.CHANGE_PAYMENT_SUBMIT_ABILITY,
    bool
  }
}

export function watchOnReconnection(callback) {
  return {
    type: CONSTANTS.WATCH_ON_RECONNECTION,
    callback
  }
}

export function sendUnSentMessages() {
  return {
    type: CONSTANTS.SEND_LEFT_MESSAGES
  }
}

export function storeJidAndNameForSupport(value) {
  return {
    type: CONSTANTS.STORE_JID_AND_NAME_FOR_SUPPORT,
    value
  }
}

export function ammendStoredMessage(value) {
  return {
    type: CONSTANTS.AMMEND_STORED_MESSAGE,
    ...value
  }
}

export function syncChatHistory(value) {
  return {
    type: CONSTANTS.SYNC_CHAT_HISTORY,
    ...value
  }
}

export function changePaymentCompleteStatus(bool) {
  return {
    type: CONSTANTS.CHANGE_PAYMENT_COMPLETE_STATUS,
    bool
  }
}

export function changeTitle(title) {
  return {
    type: CONSTANTS.CHANGE_TITLE,
    title
  }
}

export function changeErrorForNewQueryBody(error) {
  return {
    type: CONSTANTS.CHANGE_ERROR_FOR_NEW_QUERY_BODY,
    error
  }
}

export function changeOtpErrorMessage(error) {
  return {
    type: CONSTANTS.CHANGE_OTP_ERROR_MESSAGE,
    error
  }
}

export function changeQueryReSubmitAbility(bool) {
  return {
    type: CONSTANTS.CHANGE_QUERY_RE_SUBMIT_ABILITY,
    bool
  }
}

export function clearUnreadMessageCount() {
  return {
    type: CONSTANTS.CLEAR_UNREAD_MESSAGE_COUNT
  }
}

export function increaseUnreadMessageCount() {
  return {
    type: CONSTANTS.INCREASE_UNREAD_MESSAGE_COUNT
  }
}

export function stop24HourTimer() {
  return {
    type: CONSTANTS.STOP_24_HOUR_TIMER
  }
}

export function changeErrorMessageForPhoneNo(error) {
  return {
    type: CONSTANTS.CHANGE_ERROR_MESSAGE_FOR_PHONE_NO,
    error
  }
}

export function changeErrorMessageForName(error) {
  return {
    type: CONSTANTS.CHANGE_ERROR_MESSAGE_FOR_NAME,
    error
  }
}

export function changeNumber() {
  return {
    type: CONSTANTS.CHANGE_NUMBER
  }
}

export function postNewQuery() {
  return {
    type: CONSTANTS.POST_NEW_QUERY
  }
}

export function changeBottomActionButton(value) {
  return {
    type: CONSTANTS.CHANGE_BOTTOM_ACTION_BUTTON,
    value
  }
}

export function continue24HourTimer() {
  return {
    type: CONSTANTS.CONTINUE_24_HOUR_TIMER
  }
}

export function start24HourTimer(time) {
  return {
    type: CONSTANTS.START_24_HOUR_TIMER,
    time
  }
}

export function savePaymentDoneTime(time) {
  return {
    type: CONSTANTS.SAVE_PAYMENT_DONE_TIME,
    time
  }
}

export function change24hourTime(time) {
  return {
    type: CONSTANTS.CHANGE_24_HOUR_TIME,
    time
  }
}

export function change24HourTimerVisibility(bool) {
  return {
    type: CONSTANTS.CHANGE_24_HOUR_TIMER_VISIBILITY,
    bool
  }
}

export function changeResendButtonVisibility(bool) {
  return {
    type: CONSTANTS.CHANGE_RESEND_BUTTON_VISIBILITY,
    bool
  }
}

export function changeResendTick(tick) {
  return {
    type: CONSTANTS.CHANGE_RESEND_TICK,
    tick
  }
}

export function startOTPTimer() {
  return {
    type: CONSTANTS.START_OTP_TIMER
  }
}

export function submitRating(value) {
  return {
    type: CONSTANTS.SUBMIT_RATING,
    ...value
  }
}

export function changeRating(rating) {
  return {
    type: CONSTANTS.CHANGE_RATING,
    rating
  }
}

export function storeJidAndNameForDoctor(value) {
  return {
    type: CONSTANTS.STORE_JID_AND_NAME_FOR_DOCTOR,
    value
  }
}
export function closeErrorScreen() {
  return {
    type: CONSTANTS.CLOSE_ERROR_SCREEN
  }
}

export function pushError(value) {
  return {
    type: CONSTANTS.PUSH_ERROR,
    value
  }
}

export function uploadAndSendFile(value) {
  return {
    type: CONSTANTS.UPLOAD_AND_SEND_FILE,
    ...value
  }
}

export function saveQueryPaymentDetails(value) {
  return {
    type: CONSTANTS.SAVE_QUERY_PAYMENT_DETAILS,
    value
  }
}

export function getQueryPaymentDetails(value) {
  return {
    type: CONSTANTS.GET_QUERY_PAYMENT_DETAILS,
    ...value
  }
}

export function watchOnServerAck(queryId) {
  return {
    type: CONSTANTS.WATCH_ON_SERVER_ACK,
    queryId
  }
}

export function watchOnRecipientAck(queryId) {
  return {
    type: CONSTANTS.WATCH_ON_RECIPIENT_ACK,
    queryId
  }
}

export function doSingleTick(id) {
  return {
    type: CONSTANTS.DO_SINGLE_TICK,
    id
  }
}

export function doDoubleTick(id) {
  return {
    type: CONSTANTS.DO_DOUBLE_TICK,
    id
  }
}

export function callDoctorProfile(value) {
  return {
    type: CONSTANTS.CALL_DOCTOR_PROFILE,
    ...value
  }
}

export function savePromocodeDetails(value) {
  return {
    type: CONSTANTS.SAVE_PROMOCODE_DETAILS,
    value
  }
}

export function getPromocodeDetails(value) {
  return {
    type: CONSTANTS.GET_PROMOCODE_DETAILS,
    ...value
  }
}

export function saveDoctorProfileId(id) {
  return {
    type: CONSTANTS.SAVE_DOCTOR_PROFILE_ID,
    id
  }
}
export function popUpScreen(value) {
  return {
    type: CONSTANTS.POP_UP_SCREEN,
    value
  }
}

export function changePromocode(value) {
  return {
    type: CONSTANTS.CHANGE_PROMOCODE,
    value
  }
}

export function closeXmppConnection() {
  return {
    type: CONSTANTS.CLOSE_XMPP_CONNECTION
  }
}

export function openNewQuery() {
  return {
    type: CONSTANTS.OPEN_NEW_QUERY
  }
}

export function submitQueryAgain(value) {
  return {
    type: CONSTANTS.SUBMIT_QUERY_AGAIN,
    ...value
  }
}

export function submitPaymentDetails(value) {
  return {
    type: CONSTANTS.SUBMIT_PAYMENT_DETAILS,
    ...value
  }
}

export function changeReceiverJidAndName(value) {
  return {
    type: CONSTANTS.CHANGE_RECEIVER_JID_AND_NAME,
    value
  }
}

export function changeMessageValue(value) {
  return {
    type: CONSTANTS.CHANGE_MESSAGE_VALUE,
    value
  }
}

export function savePaymentDetails(value) {
  return {
    type: CONSTANTS.SAVE_PAYMENT_DETAILS,
    value
  }
}

export function saveDoctorProfile(value) {
  return {
    type: CONSTANTS.SAVE_DOCTOR_PROFILE,
    value
  }
}

export function watchOnMessage(queryId) {
  return {
    type: CONSTANTS.WATCH_ON_MESSAGE,
    queryId
  }
}

export function storeMessages(msg) {
  return {
    type: CONSTANTS.STORE_MESSAGES,
    msg
  }
}

export function sendAndStoreMessage(value) {
  return {
    type: CONSTANTS.SEND_AND_STORE_MESSAGE,
    ...value
  }
}

export function initiateXmppConnection(value) {
  return {
    type: CONSTANTS.INIT_XMPP_CONNNECTION,
    ...value
  }
}

export function changeQueryId(id) {
  return {
    type: CONSTANTS.CHANGE_QUERY_ID,
    id
  }
}

export function saveJidAndPassword(value) {
  return {
    type: CONSTANTS.SAVE_JID_AND_PASSWORD,
    value
  }
}

export function resendOTP(value) {
  return {
    type: CONSTANTS.RESEND_OTP,
    ...value
  }
}

export function confirmOTP(value) {
  return {
    type: CONSTANTS.CONFIRM_OTP,
    ...value
  }
}

export function otpSubmitAbility(bool) {
  return {
    type: CONSTANTS.CHANGE_OTP_SUBMIT_ABILITY,
    bool
  }
}

export function otpResendAbility(bool) {
  return {
    type: CONSTANTS.CHANGE_OTP_RESEND_ABILITY,
    bool
  }
}

export function changeOTPValue(value) {
  return {
    type: CONSTANTS.CHANGE_OTP_VALUE,
    value
  }
}

export function saveAuthenticationToken(value) {
  return {
    type: CONSTANTS.SAVE_AUTHENTICATION_TOKEN,
    value
  }
}

export function userVerificationStatus(bool) {
  return {
    type: CONSTANTS.SET_USER_VERIFICATION_STATUS,
    bool
  }
}

export function changeScreen(screenState) {
  return {
    type: CONSTANTS.CHANGE_SCREEN,
    screenState
  }
}

export function changeSubmitButtonAbility(bool) {
  return {
    type: CONSTANTS.CHANGE_SUBMIT_ABILITY,
    bool
  }
}

export function submitFirstScreenForm(value) {
  return {
    type: CONSTANTS.SUBMIT_FIRST_SCREEN_FORM,
    ...value
  }
}

export function changeName(value) {
  return {
    type: CONSTANTS.CHANGE_NAME,
    value
  }
}

export function changePhoneNo(value) {
  return {
    type: CONSTANTS.CHANGE_PHONENO,
    value
  }
}

export function changeQueryBody(value) {
  return {
    type: CONSTANTS.CHANGE_QUERY_BODY,
    value
  }
}

export function changeVisibilityOfChat() {
  return {
    type: CONSTANTS.CHANGE_VISIBILITY_OF_CHAT
  }
}
