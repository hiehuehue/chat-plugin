import {
  xmppConnectionInit,
  createOnMessageChannel,
  createOnRecipientAckChannel,
  createOnServerAckChannel,
  addReconnectionCallback,
  sendTextMessage,
  xmppConnectionClose,
  xmppConnectionObject
} from 'helpers/xmppConnectionHelper';

import {
  call,
  take,
  put,
  fork,
  select
} from 'redux-saga/effects';
import {
  storeMessages,
  changeReceiverJidAndName,
  saveDoctorProfileId,
  doDoubleTick,
  stop24HourTimer,
  doSingleTick,
  pushError,
  storeJidAndNameForDoctor,
  changeBottomActionButton,
  change24HourTimerVisibility,
  start24HourTimer,
  change24hourTime,
  callDoctorProfile,
  changePaymentCompleteStatus,
  changeTitle,
  closeErrorScreen,
  storeJidAndNameForSupport,
  increaseUnreadMessageCount,
  ammendStoredMessage,
  sendGA
} from 'actions';
import {
  doctorRating,
  botRating,
  uploadFile,
  chatHistory
} from 'helpers/api';
import {
  getAllMessages,
  getReceiverJidAndName,
  getJidAndNameForDoctor,
  getUserJid,
  getUserName,
  getChatPassword,
  getAllUnSentMessages,
  getJidAndNameForSupport,
  getVisibility,
  getTotalFees
} from 'helpers/selectors';
import {
  parseChatHistoryToMessagePacket
} from 'helpers/parsers';
import {
  delay
} from 'redux-saga';
import * as CONSTANTS from 'constants';

export function* sendUnSentMessages() {
  var unSentMessages = yield select(getAllUnSentMessages);
  var length = unSentMessages.length;
  while (length) {
    const {
      msgId,
      msgType,
      queryId,
      body,
      to
    } = unSentMessages[length - 1];
    let newMsgId = yield call(sendTextMessage, to, {
      msg_type: msgType,
      query_id: queryId,
      body
    });
    yield put(ammendStoredMessage({
      key: 'msgId',
      old: msgId,
      new: newMsgId
    }));
    length--;
  }
}

function* getUnAppendMessageBlock(action, time) {
  const {
    queryId,
    chatPassword
  } = action;
  var unAppendedMessages = [];
  time = time || action.time || new Date().toISOString();
  const allMessages = yield select(getAllMessages);
  try {
    var data = yield call(chatHistory, queryId, time, chatPassword);
    for (let i = 0; i < data.length; i++) {
      var found = allMessages.find((msg) => {
        return msg.msgId === data[i].msg_id;
      });
      if (found) {
        break;
      } else {
        unAppendedMessages.unshift(data[i]);
      }
    }
  } catch (e) {
    console.log(e);
    throw new Error(e);
  }
  return unAppendedMessages;
}

function* getRemainingMessageBlock(action, time, returnedMessages) {
  var unAppendedMessages = [];
  try {
    unAppendedMessages = [].concat(returnedMessages, unAppendedMessages);
    while (returnedMessages.length === 10) {
      var newTime = new Date(returnedMessages[0].inserted_at).toISOString();
      returnedMessages = yield call(getUnAppendMessageBlock, action, newTime);
      unAppendedMessages = [].concat(returnedMessages, unAppendedMessages);
    }
  } catch (e) {
    console.log(e);
  }
  return unAppendedMessages;
}

function* getAllUnAppendedMessages(action, time) {
  try {
    var returnedMessages = yield* getUnAppendMessageBlock(action, time);
    var allMessages = yield* getRemainingMessageBlock(action, time, returnedMessages);
  } catch (e) {
    yield put(pushError({
      type: CONSTANTS.RELOAD,
      e,
      warnLevel: CONSTANTS.WARN_LEVEL_RED,
      status: 'Unable to Connect. Please check your Internet Connection'
    }));
    console.log(e);
    throw new Error(e)
  }
  return allMessages;
}

function* getNewMessages(action) {
  yield delay(800);
  try {
    var unAppendedMessages = yield* getAllUnAppendedMessages(action, new Date().toISOString());
    var data = {
      username: yield select(getUserName),
      userJid: yield select(getUserJid),
      jidAndNameForSupport: yield select(getJidAndNameForSupport),
      jidAndNameForDoctor: yield select(getJidAndNameForDoctor)
    }
    var allunAppendedMessages = parseChatHistoryToMessagePacket(unAppendedMessages, data);
    var length = allunAppendedMessages.length;
    var i = 0;
    while (length) {
      yield fork(takeActionOnMessageType, allunAppendedMessages[i++], action.queryId);
      length--;
    }
  } catch (e) {
    console.log(e);
    // throw new Error(e);
  }
}

export function* syncChatHistory(action) {
  try {
    yield fork(sendUnSentMessages);
    yield fork(getNewMessages, action);
  } catch (e) {
    console.log(e);
  }
}

export function* closeXmppConnection() {
  try {
    yield call(xmppConnectionClose);
  } catch (e) {
    //FIXME: handle error internally
    console.log(e);
  }
}

export function* initiateXmppConnection(action) {
  try {
    yield call(xmppConnectionInit, {
      jid: action.jid,
      password: action.chatPassword,
      domain: CONSTANTS.domain(),
      host: CONSTANTS.host(),
      options: {
        debug: CONSTANTS.DEBUG
      }
    });
    yield fork(sendUnSentMessages);
  } catch (e) {
    // FIXME: show error in UI
    yield put(pushError({
      type: CONSTANTS.RELOAD,
      e,
      warnLevel: CONSTANTS.WARN_LEVEL_RED,
      status: 'Unable to Connect. Please check your Internet Connection'
    }));
    console.log(e);
  }
}

export function* sendAndStoreMessage(action) {
  try {
    const {
      to,
      body: {
        msg_type,
        query_id,
        body
      },
      timestamp,
      status,
      responder,
      name
    } = action;
    var msgId;
    try {
      msgId = yield call(sendTextMessage, to, action.body);
    } catch (e) {
      yield put(pushError({
        type: CONSTANTS.RELOAD,
        e,
        warnLevel: CONSTANTS.WARN_LEVEL_RED,
        status: 'Unable to Connect. Please check your Internet Connection'
      }));
      console.log(e);
    }
    yield put(storeMessages({
      msgType: msg_type,
      queryId: query_id,
      body,
      timestamp,
      status,
      responder,
      to,
      msgId,
      name
    }));
  } catch (e) {
    yield put(pushError({
      type: CONSTANTS.RELOAD,
      e,
      warnLevel: CONSTANTS.WARN_LEVEL_RED,
      status: 'Unable to Connect. Please check your Internet Connection'
    }));
    console.log(e);
  }
}

export function* watchOnReconnection(action) {
  const xmppConnectionObj = yield call(xmppConnectionObject);
  if (action.callback) {
    try {
      yield call(addReconnectionCallback, xmppConnectionObj, action.callback);
      yield put(closeErrorScreen())
    } catch (e) {
      console.log(e);
    }
  } else {
    try {
      yield call(addReconnectionCallback, xmppConnectionObj);
    } catch (e) {
      //FIXME what is happpening;
      console.log(e);
    }
  }
}

export function* watchOnServerAck(action) {
  var incomingServerAckChannel;
  if (action.queryId) {
    try {
      const xmppConnectionObj = yield call(xmppConnectionObject);
      incomingServerAckChannel = yield call(createOnServerAckChannel, xmppConnectionObj);
      while (true) {
        try {
          const id = yield take(incomingServerAckChannel);
          yield put(doSingleTick(id));
        } catch (e) {
          console.log(e);
        }
      }
    } catch (e) {
      console.log(e);
    }
  } else {
    try {
      if (incomingServerAckChannel) {
        incomingServerAckChannel.close();
      }
    } catch (e) {
      console.log(e);
    }
  }
}

export function* watchOnRecipientAck(action) {
  var incomingRecipientAckChannel;
  if (action.queryId) {
    try {
      const xmppConnectionObj = yield call(xmppConnectionObject);
      incomingRecipientAckChannel = yield call(createOnRecipientAckChannel, xmppConnectionObj);
      while (true) {
        try {
          const id = yield take(incomingRecipientAckChannel);
          yield put(doDoubleTick(id));
        } catch (e) {
          console.log(e);
        }
      }
    } catch (e) {
      console.log(e);
    }
  } else {
    try {
      if (incomingRecipientAckChannel) {
        incomingRecipientAckChannel.close();
      }
    } catch (e) {
      //FIXME what is happpening;
      console.log(e);
    }
  }
}

export function* watchOnMessage(action) {
  var incomingMessageChannel;
  if (action.queryId) {
    try {
      const xmppConnectionObj = yield call(xmppConnectionObject);
      incomingMessageChannel = yield call(createOnMessageChannel, xmppConnectionObj);
      while (true) {
        try {
          const msg = yield take(incomingMessageChannel);
          console.log(msg);
          yield fork(takeActionOnMessageType, msg, action.queryId);
        } catch (e) {
          console.log(e);
        }
      }
    } catch (e) {
      console.log(e);
    }
  } else {
    try {
      if (incomingMessageChannel) {
        incomingMessageChannel.close();
      }
    } catch (e) {
      //FIXME what is happpening;
      console.log(e);
    }
  }
}

function* takeActionOnMessageType(msg, queryId) {
  if (Number(msg.query_id) === Number(queryId)) {
    var {
      responder
    } = yield select(getReceiverJidAndName);
    switch (msg.msg_type) {
      case CONSTANTS.MSG:
      case CONSTANTS.IMAGE:
      case CONSTANTS.DOCUMENT:
        yield put(storeMessages(storeMessageFormat(msg, responder)));
        break;
      case CONSTANTS.QUERY_END:
        msg.doctorName = responder;
        debugger;
        if (msg.responder === CONSTANTS.DOCTOR) {
          yield put(sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.RESOLVED_BY_DOCTOR, CONSTANTS.QUERY_RESOLVED_BY_DOCTOR));
        } else if (msg.responder === CONSTANTS.SUPPORT) {
          yield put(sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.RESOLVED_BY_SALES, CONSTANTS.QUERY_RESOLVED_BY_SALES));
        }
        msg.responder = CONSTANTS.SUPPORT_BOT_NAME;
        yield put(storeMessages(storeMessageFormat(msg, responder)));
        yield put(change24HourTimerVisibility(false));
        yield put(changeBottomActionButton(''));
        yield put(stop24HourTimer());
        yield put(change24hourTime(CONSTANTS.HOUR_24_TIME));
        break;
      case CONSTANTS.PICKED_BY_AGENT:
        yield put(storeJidAndNameForSupport({
          responder: CONSTANTS.SUPPORT_BOT_NAME,
          jid: msg.from
        }));
        yield put(changeReceiverJidAndName({
          jid: msg.from,
          responder: CONSTANTS.SUPPORT_BOT_NAME
        }));
        break;
      case CONSTANTS.PICKED_BY_DOCTOR:
        yield put(saveDoctorProfileId(msg.id));
        msg.responder = responder;
        yield put(storeMessages(storeMessageFormat(msg, responder)));
        yield put(storeJidAndNameForDoctor({
          responder: msg.name,
          jid: msg.jid
        }));
        const chatPassword = yield select(getChatPassword);
        yield put(callDoctorProfile({
          doctorId: msg.id,
          chatPassword
        }));
        yield put(sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.DOCTOR_ASSIGNMENT, CONSTANTS.DOCTOR_ASSIGNED));
        break;
      case CONSTANTS.PAYMENT_COMPLETED:
        const data = yield select(getJidAndNameForDoctor);
        yield put(changeTitle(data.responder));
        yield put(changePaymentCompleteStatus(true));
        yield put(changeReceiverJidAndName(data));
        yield put(storeMessages(storeMessageFormat(msg, data.responder)));
        yield put(start24HourTimer(msg.timestamp));
        const totalFees = yield select(getTotalFees);
        yield put(sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.PAYMENT_SUCCESSFUL, CONSTANTS.PAYMENT_SUCCESSFUL_COMPLETION, totalFees));
        break;
      default:
        console.log(msg);
    }
    if (msg.msg_type !== CONSTANTS.PICKED_BY_AGENT && (!(yield select(getVisibility)))) {
      yield put(increaseUnreadMessageCount());
    }
  } else {
    console.log(msg);
    console.log('not from this queryId');
  }
}

function storeMessageFormat(msg, responder) {
  return {
    responder,
    ...msg,
    timestamp: msg.timestamp,
    msgType: msg.msg_type,
    msg_type: undefined,
    queryId: msg.query_id,
    query_id: undefined
  }
}

export function* submitRating(action) {
  const {
    responder,
    rating,
    chatPassword,
    queryId
  } = action;
  var newRating = rating - 1;
  try {
    if (responder === CONSTANTS.SUPPORT_BOT_NAME) {
      try {
        yield put(sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.SUBMIT_SATISFACTIONRATING, CONSTANTS.SATIFACTION_RATING_SUBMISSION));
      } catch (e) {}
      yield call(botRating, queryId, newRating, chatPassword)
    } else {
      try {
        yield put(sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.SUBMIT_DOCTORRATING, CONSTANTS.DOCTOR_RATING_SUBMISSION));
      } catch (e) {}
      yield call(doctorRating, queryId, newRating, chatPassword)
    }
    yield put(changeBottomActionButton(CONSTANTS.POST_NEW_QUERY));
  } catch (e) {
    yield put(pushError({
      type: CONSTANTS.RETRY,
      e,
      warnLevel: CONSTANTS.WARN_LEVEL_RED,
      status: 'Unable to Connect. Please check your Internet Connection'
    }));
  }
}

export function* uploadAndSendFile(action) {
  try {
    const data = yield call(uploadFile, action.file);
    action.body.body = CONSTANTS.fullDomain() + data.url;
    action.name = data.name;
    yield call(sendAndStoreMessage, action);
  } catch (e) {
    // FIXME something failed show in UI
    yield put(pushError({
      type: 'RETRY',
      e,
      warnLevel: CONSTANTS.WARN_LEVEL_RED,
      status: 'Could not send Attachements'
    }));
    console.log(e);
  }
}
