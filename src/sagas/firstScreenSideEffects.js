import {
  put,
  call
} from 'redux-saga/effects';
import {
  changeQueryReSubmitAbility,
  changeScreen,
  userVerificationStatus,
  saveAuthenticationToken,
  changeQueryId,
  storeMessages,
  changeSubmitButtonAbility,
  pushError,
  changeBottomActionButton,
  changeTitle,
  saveUserEmail
} from 'actions';
import {
  mobileVerificationStatus,
  sendOTPForVerifiedUser,
  phoneRegistration,
  querySubmission
} from 'helpers/api';
import * as CONSTANTS from 'constants';
export function* submitQueryAgain(action) {
  const {
    jidAndPassword: {
      jid,
      chatPassword
    },
    name,
    queryBody,
    phoneNo
  } = action;
  try {
    yield put(changeQueryReSubmitAbility(false));
    const query = yield call(querySubmission, jid, name, queryBody, phoneNo, chatPassword);
    yield put(changeQueryId(query.id));
    yield put(changeScreen(CONSTANTS.CHAT_SCREEN));
    yield put(changeBottomActionButton(CONSTANTS.SEND_CHAT_MESSAGE));
    yield put(changeTitle(CONSTANTS.SUPPORT_BOT_TITLE));
    yield put(storeMessages({
      msgType: CONSTANTS.MSG,
      queryId: query.id,
      body: queryBody,
      timestamp: new Date().getTime(),
      status: 2,
      responder: name,
      to: CONSTANTS.SUPPORT_BOT_JID,
      id: new Date().getTime()
    }));
  } catch (e) {
    //FIXME: SHOW ERROR IN UI;
    yield put(pushError({
      type: CONSTANTS.RETRY,
      e,
      warnLevel: CONSTANTS.WARN_LEVEL_RED,
      status: 'Unable to Connect. Please check your Internet Connection'
    }));
    yield put(changeQueryReSubmitAbility(true));
    console.log(e);
  }
  yield put(changeQueryReSubmitAbility(true));
}

export function* submitFirstScreenForm(action) {
  try {
    yield put(changeSubmitButtonAbility(false))
    const data = yield call(mobileVerificationStatus, action.phoneNo)
    if (data && data.verified) {
      yield put(userVerificationStatus(true))
      if (data.email) {
        yield put(saveUserEmail(data.email))
      }
      yield call(sendOTPForVerifiedUser, action.phoneNo)
    } else {
      yield put(userVerificationStatus(false))
      const {
        authentication_token
      } = yield call(phoneRegistration, action.name, action.phoneNo)
      yield put(saveAuthenticationToken(authentication_token))
    }
    yield put(changeScreen(CONSTANTS.OTP_VERIFICATION_SCREEN))
    yield put(changeBottomActionButton(CONSTANTS.SUBMIT_OTP_AND_QUERY))
  } catch (e) {
    // FIXME: show error in UI
    yield put(pushError({
      type: CONSTANTS.RETRY,
      e,
      warnLevel: CONSTANTS.WARN_LEVEL_RED,
      status: 'Unable to Connect. Please check your Internet Connection'
    }));
    yield put(changeSubmitButtonAbility(true));
    console.log(e);
  }
  yield put(changeSubmitButtonAbility(true));
}
