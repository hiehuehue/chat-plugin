import {
  otpSubmitAbility,
  otpResendAbility,
  saveJidAndPassword,
  saveAuthenticationToken,
  changeScreen,
  changeQueryId,
  pushError,
  changeResendTick,
  changeResendButtonVisibility,
  changeBottomActionButton,
  storeMessages,
  changeTitle,
  changeOtpErrorMessage,
  changeFirstUser
} from 'actions';
import {
  sendOTPForNonVerifiedUser,
  confirmOTPForVerifiedUser,
  confirmOTPForNonVerifiedUser,
  sendOTPForVerifiedUser,
  querySubmission,
  getJidAndPassword
} from 'helpers/api';
import {
  put,
  call,
  select,
  fork,
  cancel,
  take
} from 'redux-saga/effects';
import {
  delay
} from 'redux-saga';
import {
  getResendTick
} from 'helpers/selectors';
import {
  sendGA
} from 'helpers/ga';
import * as CONSTANTS from 'constants';
export function* confirmOTP(action) {
  try {
    yield put(otpSubmitAbility(false));
    const {
      userVerificationStatus,
      phoneNo,
      otp,
      name,
      authenticationToken,
      queryBody
    } = action;
    var isClientError = false;
    if (userVerificationStatus) {
      try {
        var {
          authentication_token,
          jid,
          chat_password
        } = yield call(confirmOTPForVerifiedUser, phoneNo, otp);
      } catch (e) {
        console.log(e);
        yield put(changeOtpErrorMessage('OTP could not be verified'));
        isClientError = true;
        throw e;
      }
      yield put(saveAuthenticationToken(authentication_token));
      if (!jid) {
        var newJidAndPassword = yield call(getJidAndPassword, authentication_token);
        jid = newJidAndPassword.jid;
        chat_password = newJidAndPassword.chat_password;
      }
    } else {
      try {
        var {
          jid,
          chat_password
        } = yield call(confirmOTPForNonVerifiedUser, phoneNo, otp, authenticationToken);
        if (!jid) {
          var newJidAndPassword = yield call(getJidAndPassword, authentication_token);
          jid = newJidAndPassword.jid;
          chat_password = newJidAndPassword.chat_password;
        }
      } catch (e) {
        console.log(e);
        yield put(changeOtpErrorMessage('Otp could not be confirmed, Please try Again'));
        isClientError = true;
        throw e;
      }
    }
    yield put(saveJidAndPassword({
      jid,
      chatPassword: chat_password
    }));
    const query = yield call(querySubmission, jid, name, queryBody, phoneNo, chat_password);
    yield call(sendGA, CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.CLICK_SUBMIT, CONSTANTS.FIRST_QUERY_SUBMIT);
    yield put(changeFirstUser());
    yield put(changeQueryId(query.id));
    yield put(changeScreen(CONSTANTS.CHAT_SCREEN));
    yield put(changeBottomActionButton(CONSTANTS.SEND_CHAT_MESSAGE));
    yield put(changeTitle(CONSTANTS.SUPPORT_BOT_TITLE));
    yield put(storeMessages({
      msgType: CONSTANTS.MSG,
      queryId: query.id,
      body: action.queryBody,
      timestamp: new Date().getTime(),
      status: 2,
      responder: name,
      to: CONSTANTS.SUPPORT_BOT_JID,
      id: new Date().getTime()
    }));
  } catch (e) {
    // FIXME: show error in UI
    if (!isClientError) {
      yield put(pushError({
        type: CONSTANTS.RETRY,
        e,
        warnLevel: CONSTANTS.WARN_LEVEL_RED,
        status: 'OTP could not be submitted, Check OTP and try again'
      }));
      yield call(sendGA, CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.OTP_FAILED, CONSTANTS.OTP_FAILURE)
      yield put(otpSubmitAbility(true));
      console.log(e);
    }
  }
  yield put(otpSubmitAbility(true));
}

function* otpTimer() {
  while (true) {
    yield call(delay, 1000);
    const resendTick = yield select(getResendTick);
    const timeleft = resendTick - 1;
    yield put(changeResendTick(timeleft));
    if (timeleft === 0) {
      yield put(changeResendButtonVisibility(true));
      break;
    }
  }
}

export function* startOTPTimer() {
  try {
    yield put(changeResendButtonVisibility(false));
    yield put(changeResendTick(CONSTANTS.RESEND_TIME))
    const bgOtpTimer = yield fork(otpTimer);
    yield take(CONSTANTS.CHANGE_SCREEN);
    yield cancel(bgOtpTimer);
  } catch (e) {
    console.log(e);
  }
}

export function* resendOTP(action) {
  try {
    yield put(otpResendAbility(false));
    if (action.userVerificationStatus) {
      yield call(sendOTPForVerifiedUser, action.phoneNo)
    } else {
      yield call(sendOTPForNonVerifiedUser, action.phoneNo, action.authenticationToken)
    }
    yield put(otpResendAbility(true));
  } catch (e) {
    // FIXME: show error in UI
    yield put(pushError({
      type: CONSTANTS.RETRY,
      e,
      warnLevel: CONSTANTS.WARN_LEVEL_RED,
      status: 'OTP could not be resent, Please resend Again'
    }));
    yield put(otpResendAbility(true));
    console.log(e);
  }
}
