import {
  saveDoctorProfile
} from 'actions';
import {
  showDoctorProfile
} from 'helpers/api';
import {
  put,
  call,
  takeLatest,
  takeEvery,
  fork
} from 'redux-saga/effects';
import {
  initiateXmppConnection,
  sendAndStoreMessage,
  watchOnMessage,
  watchOnRecipientAck,
  watchOnServerAck,
  watchOnReconnection,
  closeXmppConnection,
  submitRating,
  uploadAndSendFile,
  syncChatHistory,
  sendUnSentMessages
} from './chatScreenSideEffects';
import {
  confirmOTP,
  resendOTP,
  startOTPTimer
} from './otpSideEffects';
import {
  submitFirstScreenForm,
  submitQueryAgain
} from './firstScreenSideEffects';
import {
  getQueryPaymentDetails,
  submitPaymentDetails,
  continue24HourTimer,
  start24HourTimer,
  callPromocodeDetails
} from './paymentSideEffects';
import {
  sendGA
} from 'helpers/ga';
import * as CONSTANTS from 'constants';

function* callDoctorProfile(action) {
  try {
    const data = yield call(showDoctorProfile, action.doctorId, action.chatPassword)
    yield put(saveDoctorProfile(data))
  } catch (e) {
    // FIXME: show error in UI
    console.log(e);
  }
}

function* callGA(action) {
  try {
    const {
      eventCategory,
      eventAction,
      eventLabel,
      value
    } = action;
    yield call(sendGA, eventCategory, eventAction, eventLabel, value);
  } catch (e) {
    console.log(e);
  }
}

export default () => [
  takeLatest(CONSTANTS.SUBMIT_FIRST_SCREEN_FORM, submitFirstScreenForm),
  takeLatest(CONSTANTS.CONFIRM_OTP, confirmOTP),
  takeLatest(CONSTANTS.RESEND_OTP, resendOTP),
  takeLatest(CONSTANTS.INIT_XMPP_CONNNECTION, initiateXmppConnection),
  takeLatest(CONSTANTS.WATCH_ON_MESSAGE, watchOnMessage),
  takeLatest(CONSTANTS.WATCH_ON_RECIPIENT_ACK, watchOnRecipientAck),
  takeLatest(CONSTANTS.WATCH_ON_SERVER_ACK, watchOnServerAck),
  takeLatest(CONSTANTS.SEND_AND_STORE_MESSAGE, sendAndStoreMessage),
  takeLatest(CONSTANTS.SUBMIT_QUERY_AGAIN, submitQueryAgain),
  takeLatest(CONSTANTS.CLOSE_XMPP_CONNECTION, closeXmppConnection),
  takeLatest(CONSTANTS.CALL_DOCTOR_PROFILE, callDoctorProfile),
  takeLatest(CONSTANTS.SUBMIT_PAYMENT_DETAILS, submitPaymentDetails),
  takeEvery(CONSTANTS.UPLOAD_AND_SEND_FILE, uploadAndSendFile),
  takeLatest(CONSTANTS.GET_PROMOCODE_DETAILS, callPromocodeDetails),
  takeLatest(CONSTANTS.GET_QUERY_PAYMENT_DETAILS, getQueryPaymentDetails),
  takeLatest(CONSTANTS.SUBMIT_RATING, submitRating),
  takeLatest(CONSTANTS.START_OTP_TIMER, startOTPTimer),
  takeLatest(CONSTANTS.START_24_HOUR_TIMER, start24HourTimer),
  takeLatest(CONSTANTS.CONTINUE_24_HOUR_TIMER, continue24HourTimer),
  takeLatest(CONSTANTS.SYNC_CHAT_HISTORY, syncChatHistory),
  takeLatest(CONSTANTS.SEND_LEFT_MESSAGES, sendUnSentMessages),
  takeLatest(CONSTANTS.WATCH_ON_RECONNECTION, watchOnReconnection),
  takeEvery(CONSTANTS.SEND_GA, callGA)
]
