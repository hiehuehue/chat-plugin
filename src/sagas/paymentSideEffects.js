import {
  openModal
} from 'helpers/razorpay';
import {
  getPaymentDetails,
  completePayment,
  makeZeroPayment,
  getQueryPaymentDetails as callQueryPaymentDetails,
  getPromocodeDetails
} from 'helpers/api';
import {
  savePaymentDetails,
  pushError,
  saveQueryPaymentDetails,
  popUpScreen,
  savePaymentDoneTime,
  change24HourTimerVisibility,
  change24hourTime,
  savePromocodeDetails,
  changeBottomActionButton,
  changePaymentSubmitAbility,
  changeLoadingScreenVisibility,
  sendGA
} from 'actions';
import {
  getPaymentDoneTime,
  getHour24Timer
} from 'helpers/selectors';
import {
  timeRemainingfrom24
} from 'helpers/timestamp';
import {
  put,
  call,
  fork,
  take,
  cancel,
  select
} from 'redux-saga/effects';
import {
  delay
} from 'redux-saga';
import * as CONSTANTS from 'constants';
import {
  getState
} from 'helpers/selectors';
export function* start24HourTimer(action) {
  try {
    const {
      time
    } = action;
    yield put(savePaymentDoneTime(time));
    yield fork(continue24HourTimer);
  } catch (e) {
    console.log(e);
  }
}

export function* continue24HourTimer() {
  try {
    yield put(change24HourTimerVisibility(true));
    const bgHour24Timer = yield fork(hour24Timer);
    yield take(CONSTANTS.STOP_24_HOUR_TIMER);
    yield cancel(bgHour24Timer);
    yield put(change24HourTimerVisibility(false));
  } catch (e) {
    console.log(e);
  }
}

function* hour24Timer() {
  try {
    while (true) {
      const time = yield select(getPaymentDoneTime);
      const newTime = CONSTANTS.HOUR_24_TIME - (new Date().getTime() - time);
      const oldTime = yield select(getHour24Timer);
      const changedTime = timeRemainingfrom24(newTime);
      if (changedTime && changedTime !== oldTime) {
        yield put(change24hourTime(changedTime));
      }
      if (newTime < 0) {
        yield put(change24HourTimerVisibility(false));
        break;
      }
      yield delay(1000);
    }
  } catch (e) {
    console.log(e);
  }
}

export function* getQueryPaymentDetails(action) {
  try {
    const data = yield call(callQueryPaymentDetails, action.queryId, action.chatPassword)
    yield put(saveQueryPaymentDetails(data))
  } catch (e) {
    // FIXME: show error in UI
    console.log(e);
  }
}

function* tryCompletePayment(queryId, chatPassword) {
  var i = 0;
  while (true) {
    try {
      yield call(completePayment, queryId, chatPassword);
      break;
    } catch (e) {
      console.log(e);
      yield delay(1000);
      i++;
      if (i === 10) {
        yield put(changePaymentSubmitAbility(true));
        yield put(pushError({
          type: CONSTANTS.RELOAD,
          warnLevel: CONSTANTS.WARN_LEVEL_RED,
          status: 'Some Error on Our Side, Please contact PSTakeCare'
        }));
        var state = yield select(getState);
        yield put(sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.PAYMENTAPI_FAILED, CONSTANTS.PAYMENT_API_FAILED, state));
        break;
      }
    }
  }
}
export function* submitPaymentDetails(action) {
  yield put(changePaymentSubmitAbility(false));
  yield put(changeLoadingScreenVisibility(true));
  try {
    const {
      queryId,
      chatPassword,
      phoneNo,
      name,
      email,
      promoCodeDetails,
      queryPaymentDetails
    } = action;
    var promoDiscount = Number(promoCodeDetails && promoCodeDetails.discount)
    var initialAmount = Number(queryPaymentDetails && queryPaymentDetails.total_fees)
    if (initialAmount - promoDiscount === 0) {
      const paymentDetails = yield call(makeZeroPayment, queryId, chatPassword, promoCodeDetails)
      yield put(savePaymentDetails(paymentDetails));
    } else {
      const paymentDetails = yield call(getPaymentDetails, queryId, chatPassword, promoCodeDetails)
      yield put(savePaymentDetails(paymentDetails))
      yield call(openModal, {
        phoneNo,
        name,
        email
      }, paymentDetails);
      yield call(tryCompletePayment, queryId, chatPassword);
    }
    yield put(changeBottomActionButton(CONSTANTS.SEND_CHAT_MESSAGE));
    yield put(popUpScreen(''));
  } catch (e) {
    // FIXME: show error in UI
    if (e.err && e.err === 'dismissed modal') {} else {
      yield put(pushError({
        type: CONSTANTS.RETRY,
        e: JSON.stringify(e),
        warnLevel: CONSTANTS.WARN_LEVEL_RED,
        status: 'Unable to Connect. Please check your Internet Connection'
      }));
    }
    console.log(e);
  }
  yield put(changePaymentSubmitAbility(true));
  yield put(changeLoadingScreenVisibility(false));
}
export function* callPromocodeDetails(action) {
  try {
    const data = yield call(getPromocodeDetails, action.queryId, action.promoCode, action.chatPassword);
    yield put(savePromocodeDetails(data));
    if (data.success) {
      yield put(sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.PROMOCODE_SUCCESSFUL, CONSTANTS.PROMOCODE_APPLIED));
    } else {
      yield put(sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.PROMOCODE_FAILED, CONSTANTS.PROMOCODE_FAILURE));
    }
  } catch (e) {
    // FIXME: show error in UI
    console.log(e);
  }
}
