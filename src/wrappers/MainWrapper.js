import {
  connect
} from 'react-redux';
import {
  changeVisibilityOfChat
} from 'actions';
import Main from 'components/Main';

function mapStateToProps(state) {
  return {
    visibility: state.visibility
  }
}

function mapDispatchToProps(dispatch) {
  return {
    changeVisibilityOfChat: () => dispatch(changeVisibilityOfChat())
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
