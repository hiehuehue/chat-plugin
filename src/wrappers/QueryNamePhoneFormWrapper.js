import {
  connect
} from 'react-redux';
import QueryNamePhoneForm from 'components/QueryNamePhoneForm';

import {
  changeName,
  changePhoneNo,
  changeQueryBody,
  changeErrorForNewQueryBody,
  changeErrorMessageForName,
  changeErrorMessageForPhoneNo,
  changeSubmitButtonAbility
} from 'actions';

function mapStateToProps(state) {
  return {
    name: state.name,
    phoneNo: state.phoneNo,
    queryBody: state.queryBody,
    errorForName: state.errorForName,
    errorForPhoneNo: state.errorForPhoneNo,
    errorForNewQueryBody: state.errorForNewQueryBody
  }
}

function mapDispatchToProps(dispatch) {
  return {
    changeName: (value) => dispatch(changeName(value)),
    changePhoneNo: (value) => dispatch(changePhoneNo(value)),
    changeQueryBody: (value) => dispatch(changeQueryBody(value)),
    changeErrorMessageForName: (value) => dispatch(changeErrorMessageForName(value)),
    changeErrorMessageForPhoneNo: (value) => dispatch(changeErrorMessageForPhoneNo(value)),
    changeSubmitButtonAbility: (value) => dispatch(changeSubmitButtonAbility(value)),
    changeErrorForNewQueryBody: (value) => dispatch(changeErrorForNewQueryBody(value))
  }
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QueryNamePhoneForm);
