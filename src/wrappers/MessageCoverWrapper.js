import {
  connect
} from 'react-redux';
import MessageCover from 'components/MessageCover';

function mapStateToProps(state) {
  return {
    jid: state.jidAndPassword.jid,
    name: state.name
  }
}

export default connect(
  mapStateToProps
)(MessageCover);
