import {
  connect
} from 'react-redux';
import {
  changeOTPValue,
  resendOTP,
  startOTPTimer,
  changeNumber,
  changeOtpErrorMessage,
  otpSubmitAbility,
  sendGA
} from 'actions';
import OtpVerification from 'components/OtpVerification';

function mapStateToProps(state) {
  return {
    phoneNo: state.phoneNo,
    otp: state.otp,
    userVerificationStatus: state.userVerificationStatus,
    authenticationToken: state.authenticationToken,
    otpResendAbility: state.otpResendAbility,
    resendButtonVisibility: state.resendButtonVisibility,
    resendTick: state.resendTick,
    otpErrorMessage: state.otpErrorMessage
  }
}

function mapDispatchToProps(dispatch) {
  return {
    changeOTPValue: (value) => dispatch(changeOTPValue(value)),
    resendOTP: (value) => dispatch(resendOTP(value)),
    startOTPTimer: () => dispatch(startOTPTimer()),
    changeNumber: (value) => dispatch(changeNumber(value)),
    changeOtpErrorMessage: (value) => dispatch(changeOtpErrorMessage(value)),
    otpSubmitAbility: (value) => dispatch(otpSubmitAbility(value)),
    sendGA: (...args) => dispatch(sendGA(...args))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OtpVerification);
