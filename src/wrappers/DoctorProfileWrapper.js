import {
  connect
} from 'react-redux';
import {
  callDoctorProfile,
  popUpScreen
} from 'actions';
import DoctorProfile from 'components/DoctorProfile';

function mapStateToProps(state) {
  return {
    doctorProfile: state.doctorProfile,
    doctorId: state.doctorId,
    chatPassword: state.jidAndPassword.chatPassword
  }
}

function mapDispatchToProps(dispatch) {
  return {
    callDoctorProfile: (value) => dispatch(callDoctorProfile(value)),
    popUpScreen: (bool) => dispatch(popUpScreen(bool))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DoctorProfile);
