import {
  connect
} from 'react-redux';
import ChatBox from 'components/ChatBox';
import {
  submitFirstScreenForm,
  confirmOTP,
  changeErrorMessageForPhoneNo,
  changeErrorMessageForName,
  openNewQuery,
  submitQueryAgain,
  submitPaymentDetails,
  changeErrorForNewQueryBody,
  clearUnreadMessageCount,
  changePaymentSubmitAbility,
  sendGA
} from 'actions';

function mapStateToProps(state) {
  return {
    visibility: state.visibility,
    errors: state.errors,
    submitAbility: state.submitAbility,
    errorForName: state.errorForName,
    errorForPhoneNo: state.errorForPhoneNo,
    paymentCompleteStatus: state.paymentCompleteStatus,
    queryReSubmitAbility: state.queryReSubmitAbility,
    name: state.name,
    phoneNo: state.phoneNo,
    email: state.email,
    queryBody: state.queryBody,
    bottomActionButton: state.bottomActionButton,
    otp: state.otp,
    otpErrorMessage: state.otpErrorMessage,
    jidAndPassword: state.jidAndPassword,
    userVerificationStatus: state.userVerificationStatus,
    authenticationToken: state.authenticationToken,
    otpSubmitAbility: state.otpSubmitAbility,
    queryPaymentDetails: state.queryPaymentDetails,
    promoCodeDetails: state.promoCodeDetails,
    queryId: state.queryId,
    unreadMessageCount: state.unreadMessageCount,
    paymentSubmitAbility: state.paymentSubmitAbility
  }
}

function mapDispatchToProps(dispatch) {
  return {
    sendGA: (...args) => dispatch(sendGA(...args)),
    changeErrorForNewQueryBody: (value) => dispatch(changeErrorForNewQueryBody(value)),
    submitPaymentDetails: (value) => dispatch(submitPaymentDetails(value)),
    confirmOTP: (value) => dispatch(confirmOTP(value)),
    submitFirstScreenForm: (value) => dispatch(submitFirstScreenForm(value)),
    openNewQuery: () => dispatch(openNewQuery()),
    submitQueryAgain: (value) => dispatch(submitQueryAgain(value)),
    clearUnreadMessageCount: (value) => dispatch(clearUnreadMessageCount(value)),
    changePaymentSubmitAbility: (value) => dispatch(changePaymentSubmitAbility(value)),
    changeErrorMessageForPhoneNo: (value) => dispatch(changeErrorMessageForPhoneNo(value)),
    changeErrorMessageForName: (value) => dispatch(changeErrorMessageForName(value))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatBox);
