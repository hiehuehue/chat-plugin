import {
  connect
} from 'react-redux';
import Rating from 'components/Rating';

import {
  changeRating,
  submitRating
} from 'actions';

function mapStateToProps(state) {
  return {
    rating: state.rating,
    responder: state.receiverJidAndName.responder,
    chatPassword: state.jidAndPassword.chatPassword,
    queryId: state.queryId,
    bottomActionButton: state.bottomActionButton
  }
}

function mapDispatchToProps(dispatch) {
  return {
    changeRating: (value) => dispatch(changeRating(value)),
    submitRating: (value) => dispatch(submitRating(value))
  }
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Rating);
