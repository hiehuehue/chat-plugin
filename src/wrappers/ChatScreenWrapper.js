import {
  connect
} from 'react-redux';
import ChatScreen from 'components/ChatScreen';
import {
  initiateXmppConnection,
  sendTextMessage,
  addMessageToUi,
  watchOnMessage,
  watchOnRecipientAck,
  watchOnServerAck,
  closeXmppConnection,
  continue24HourTimer,
  syncChatHistory,
  sendUnSentMessages,
  watchOnReconnection
} from 'actions';

function mapStateToProps(state) {
  return {
    jidAndPassword: state.jidAndPassword,
    allMessages: state.allMessages,
    queryId: state.queryId,
    hour24TimerVisibility: state.hour24TimerVisibility
  }
}

function mapDispatchToProps(dispatch) {
  return {
    initiateXmppConnection: (value) => dispatch(initiateXmppConnection(value)),
    watchOnMessage: (value) => dispatch(watchOnMessage(value)),
    watchOnRecipientAck: (value) => dispatch(watchOnRecipientAck(value)),
    watchOnServerAck: (value) => dispatch(watchOnServerAck(value)),
    sendTextMessage: (value) => dispatch(sendTextMessage(value)),
    addMessageToUi: (value) => dispatch(addMessageToUi(value)),
    closeXmppConnection: (value) => dispatch(closeXmppConnection(value)),
    continue24HourTimer: () => dispatch(continue24HourTimer()),
    syncChatHistory: (value) => dispatch(syncChatHistory(value)),
    sendUnSentMessages: () => dispatch(sendUnSentMessages()),
    watchOnReconnection: (callback) => dispatch(watchOnReconnection(callback))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatScreen);
