import {
  connect
} from 'react-redux';
import {
  getPromocodeDetails,
  changePromocode,
  getQueryPaymentDetails,
  savePromocodeDetails
} from 'actions';
import PaymentScreen from 'components/PaymentScreen';

function mapStateToProps(state) {
  // Add email
  return {
    queryId: state.queryId,
    chatPassword: state.jidAndPassword.chatPassword,
    phoneNo: state.phoneNo,
    name: state.name,
    promoCode: state.promoCode,
    promoCodeDetails: state.promoCodeDetails,
    queryPaymentDetails: state.queryPaymentDetails
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getPromocodeDetails: (value) => dispatch(getPromocodeDetails(value)),
    changePromocode: (value) => dispatch(changePromocode(value)),
    getQueryPaymentDetails: (value) => dispatch(getQueryPaymentDetails(value)),
    savePromocodeDetails: (value) => dispatch(savePromocodeDetails(value))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PaymentScreen);
