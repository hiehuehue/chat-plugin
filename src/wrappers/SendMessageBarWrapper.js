import {
  connect
} from 'react-redux';
import {
  changeMessageValue,
  uploadAndSendFile,
  sendAndStoreMessage,
  pushError
} from 'actions';
import SendMessageBar from 'components/SendMessageBar';

function mapStateToProps(state) {
  return {
    message: state.message,
    receiverJidAndName: state.receiverJidAndName,
    queryId: state.queryId,
    name: state.name
  }
}

function mapDispatchToProps(dispatch) {
  return {
    changeMessageValue: (value) => dispatch(changeMessageValue(value)),
    sendAndStoreMessage: (value) => dispatch(sendAndStoreMessage(value)),
    uploadAndSendFile: (value) => dispatch(uploadAndSendFile(value)),
    pushError: (value) => dispatch(pushError(value))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SendMessageBar);
