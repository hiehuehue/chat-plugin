import {
  connect
} from 'react-redux';
import {
  changeQueryBody,
  changeErrorForNewQueryBody,
  changeQueryReSubmitAbility
} from 'actions';
import NewQueryScreen from 'components/NewQueryScreen';

function mapStateToProps(state) {
  return {
    name: state.name,
    queryBody: state.queryBody,
    phoneNo: state.phoneNo,
    submitAbility: state.submitAbility,
    errorForNewQueryBody: state.errorForNewQueryBody
  }
}

function mapDispatchToProps(dispatch) {
  return {
    changeQueryReSubmitAbility: (value) => dispatch(changeQueryReSubmitAbility(value)),
    changeQueryBody: (value) => dispatch(changeQueryBody(value)),
    changeErrorForNewQueryBody: (value) => dispatch(changeErrorForNewQueryBody(value))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewQueryScreen);
