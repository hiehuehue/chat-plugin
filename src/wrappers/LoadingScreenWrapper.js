import {
  connect
} from 'react-redux';
import {
  changeLoadingScreenVisibility
} from 'actions';
import LoadingScreen from 'components/LoadingScreen';

function mapStateToProps(state) {
  return {
    loadingScreenVisibility: state.loadingScreenVisibility
  }
}

function mapDispatchToProps(dispatch) {
  return {
    changeLoadingScreenVisibility: (value) => dispatch(changeLoadingScreenVisibility(value))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoadingScreen);
