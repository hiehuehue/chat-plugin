import {
  connect
} from 'react-redux';
import MainContainer from 'components/MainContainer';

function mapStateToProps(state) {
  return {
    screenState: state.screenState,
    popUpScreenName: state.popUpScreenName
  }
}

export default connect(
  mapStateToProps
)(MainContainer);
