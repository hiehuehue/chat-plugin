import {
  connect
} from 'react-redux';
import {
  closeErrorScreen
} from 'actions';
import ErrorComponent from 'components/ErrorComponent';

function mapStateToProps(state) {
  return {
    errors: state.errors
  }
}

function mapDispatchToProps(dispatch) {
  return {
    closeErrorScreen: () => dispatch(closeErrorScreen())
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ErrorComponent);
