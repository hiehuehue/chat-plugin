import {
  connect
} from 'react-redux';
import {
  submitPaymentDetails,
  popUpScreen,
  sendGA,
  changeBottomActionButton
} from 'actions';
import MessageContainer from 'components/MessageContainer';

function mapStateToProps(state) {
  return {
    queryId: state.queryId,
    phoneNo: state.phoneNo,
    name: state.name,
    doctorProfile: state.doctorProfile,
    chatPassword: state.jidAndPassword.chatPassword,
    paymentCompleteStatus: state.paymentCompleteStatus,
    responder: state.receiverJidAndName.responder
  }
}

function mapDispatchToProps(dispatch) {
  return {
    submitPaymentDetails: (value) => dispatch(submitPaymentDetails(value)),
    popUpScreen: (value) => dispatch(popUpScreen(value)),
    changeBottomActionButton: (value) => dispatch(changeBottomActionButton(value)),
    sendGA: (...args) => dispatch(sendGA(...args))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MessageContainer);
