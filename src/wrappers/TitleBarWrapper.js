import {
  connect
} from 'react-redux';
import {
  changeVisibilityOfChat,
  popUpScreen,
  changeBottomActionButton,
  clearUnreadMessageCount,
  savePromocodeDetails,
  changePromocode,
  changeToInitiated,
  sendGA
} from 'actions';
import TitleBar from 'components/TitleBar';

function mapStateToProps(state) {
  return {
    popUpScreenName: state.popUpScreenName,
    visibility: state.visibility,
    responder: state.receiverJidAndName.responder,
    hour24Timer: state.hour24Timer,
    hour24TimerVisibility: state.hour24TimerVisibility,
    unreadMessageCount: state.unreadMessageCount,
    title: state.title,
    doctorProfile: state.doctorProfile,
    isFirstUser: state.isFirstUser,
    isUnInitiated: state.isUnInitiated,
    screenState: state.screenState
  }
}

function mapDispatchToProps(dispatch) {
  return {
    changeToInitiated: () => dispatch(changeToInitiated()),
    changeVisibilityOfChat: (value) => dispatch(changeVisibilityOfChat(value)),
    popUpScreen: (value) => dispatch(popUpScreen(value)),
    changeBottomActionButton: (value) => dispatch(changeBottomActionButton(value)),
    clearUnreadMessageCount: () => dispatch(clearUnreadMessageCount()),
    savePromocodeDetails: (value) => dispatch(savePromocodeDetails(value)),
    changePromocode: (value) => dispatch(changePromocode(value)),
    sendGA: (...args) => dispatch(sendGA(...args))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TitleBar);
