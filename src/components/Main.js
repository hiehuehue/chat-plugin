import React from 'react';
import ChatBoxWrapper from 'wrappers/ChatBoxWrapper';
import TitleBarWrapper from 'wrappers/TitleBarWrapper';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Frame from 'react-frame-component';
import LoadingScreenWrapper from 'wrappers/LoadingScreenWrapper';
import * as CONSTANTS from 'constants';
import {createGAandSendPageView} from 'helpers/ga';
class Main extends React.Component {
  renderContent() {
    return (<MuiThemeProvider>
      <div style={this.props.orientation === 'desktop'? flexWrapper: this.props.orientation === 'mobile' ? flexWrapperMobile: ''}>
        <TitleBarWrapper/>
        <ChatBoxWrapper/>
        <LoadingScreenWrapper/>
      </div>
    </MuiThemeProvider>)
  }
  componentDidMount() {
    if(this.props.orientation === 'mobile'){
      if(this.props.visibility){
        document.body.style.overflow='hidden';
      }else {
        document.body.style.overflow='auto';
      }
    }
    createGAandSendPageView(CONSTANTS.WEBSITE_GA_ID);
  }
  componentDidUpdate(prevProps) {
    if(this.props.orientation === 'mobile'){
      if(!prevProps.visibility){
        document.body.style.overflow='hidden';
      }else {
        document.body.style.overflow='auto';
      }
    }
  }
  render() {
    const {orientation, visibility} = this.props;
    const isMobileAndClosed = !visibility && orientation === 'mobile';
    const isMobileAndOpen = visibility && orientation === 'mobile';
    const isDesktopAndClosed = !visibility && orientation === 'desktop';
    const isDesktopAndOpen = visibility && orientation === 'desktop';
    if(process.env.NODE_ENV === 'development'){
        return (
        <div style={
          isMobileAndOpen ? MainVisibilityMobile:
          isMobileAndClosed ? MainInVisibilityMobile :
          isDesktopAndOpen ? MainVisibilityDesktop :
          isDesktopAndClosed ? MainInvisibilityDesktop : ''}>
          <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet"/>
          {this.renderContent()}
        </div>
      );
    } else {
      const url = PQUERY_ASSETS + 'assets/styles.css'; // eslint-disable-line no-undef
      return (
        <Frame initialContent='<!DOCTYPE html><html style="overflow:hidden"><head><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"><link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet"/></head><body style="margin:0;padding:0;"><div></div></body></html>' style={
          isMobileAndOpen ? MainVisibilityMobile:
          isMobileAndClosed ? MainInVisibilityMobile :
          isDesktopAndOpen ? MainVisibilityDesktop :
          isDesktopAndClosed ? MainInvisibilityDesktop : ''} head={
          <link type='text/css' rel='stylesheet' href={url} />
        } id="root">
        {this.renderContent()}
      </Frame>
    );
    }
  }
}
const MainInVisibilityMobile = {
  display: 'flex',
  justifyContent: 'flex-end',
  position: 'fixed',
  bottom:0,
  left: 0,
  height: '50px',
  background: 'none',
  display: 'block',
  width: '100%',
  border: '0 none transparent',
  margin: '0',
  padding: '0',
  overflow: 'hidden',
  transition: 'height 0.2s'
}
const MainVisibilityMobile = {
  display: 'flex',
  justifyContent: 'flex-end',
  height: '100%',
  width: '100%',
  position: 'fixed',
  bottom:0,
  left: 0,
  background: 'none',
  display: 'block',
  border: '0 none transparent',
  margin: '0px',
  padding: '0px',
  overflow: 'hidden',
  transition: 'height 0.2s'
}
const MainVisibilityDesktop = {
  position: 'relative',
  background: 'none',
  display: 'block',
  border: '0 none transparent',
  margin: '0px',
  padding: '0px',
  height: '450px',
  boxShadow: 'border-box',
  width: '320px',
  overflow: 'hidden',
  transition: 'height 0.2s'
}
const MainInvisibilityDesktop = {
  position: 'relative',
  background: 'none',
  display: 'block',
  border: '0 none transparent',
  margin: '0',
  padding: '0',
  width: '320px',
  height: '50px',
  overflow: 'hidden',
  transition: 'height 0.2s'
}

const flexWrapper = {
  display: 'flex',
  margin: '0',
  boxShadow: '0px 0px 2px 2px rgba(0, 0, 0, 0.2)',
  flexDirection: 'column',
  alignItems: 'flex-end',
  border: '1px solid #dfdfdf',
  width: '320px',
  height: '450px',
  borderRadius: '2px',
  boxSizing: 'border-box'
}
const flexWrapperMobile = {
  display: 'flex',
  margin: '0',
  boxShadow: '0px 0px 2px 2px rgba(0, 0, 0, 0.2)',
  flexDirection: 'column',
  alignItems: 'flex-end',
  borderRadius: '2px',
  position: 'fixed',
  width: '100%',
  bottom: '0',
  left: '0',
  height: '100%',
  justifyContent: 'flex-end'
}
export default Main;
