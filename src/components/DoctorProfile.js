import React from 'react'
import styles from 'styles/DoctorProfile.scss';
import CSSModules from 'react-css-modules';
import DefaultDoctorPng from 'images/doctor-placeholder.png';

const DoctorProfile = React.createClass({
  imageUrl() {
    return this.props.doctorProfile.profile_pic === 'error'
      ? <div styleName='doctor-image'><img src={DefaultDoctorPng} alt="default-image-doctor" styleName='default-image'/></div>
      : <div styleName='doctor-image'>
        <div styleName='image-wrapper'>
          <img src={this.props.doctorProfile.profile_pic}></img>
        </div>
      </div>
  },
  prefix() {
    return this.props.doctorProfile.prefix
  },
  name() {
    return this.props.doctorProfile.name
  },
  specialities() {
    return this.props.doctorProfile.specialities.map((speciality, i) => <div key={i}>{speciality.name}</div>)
  },
  experience() {
    if (!this.props.doctorProfile)
      return 0;
    var years = this.props.doctorProfile.experience
    return years + ' Year' + ((years > 1)
      ? 's'
      : '') + ' of Experience'
  },
  experienceList() {
    return this.props.doctorProfile.experience_details.map((experience, i) => <div styleName='small-block' key={i}>
      <div styleName='small-block-left'>
        <div styleName='name'>{experience.hospital}</div>
        <div styleName='desc'>{experience.locality}, {experience.city}</div>
      </div>
      <div styleName='small-block-right'>{experience.from}-{experience.to}</div>
    </div>)
  },
  educationList() {
    return this.props.doctorProfile.education_details.map((education, i) => <div styleName='small-block' key={i}>
      <div styleName='small-block-left'>
        <div styleName='name'>{education.degree.name}</div>
        <div styleName='desc'>{education.college}</div>
      </div>
      <div styleName='small-block-right'>
        {education.from}-{education.to}</div>
    </div>)
  },
  componentWillMount() {
    const {doctorId, chatPassword, callDoctorProfile} = this.props;
    callDoctorProfile({doctorId, chatPassword});
  },
  renderContent() {
    return Object.keys(this.props.doctorProfile).length === 0
      ? <div>Loading</div>
      : (
        <div>
          <div styleName="doctor-first-fold">
            {this.imageUrl()}
            <div styleName='doctor-profile-name'>{this.prefix()} {this.name()}</div>
            <div styleName='doctor-spec'>{this.specialities()}</div>
            <div styleName='doctor-exp'>{this.experience()}</div>
          </div>
          <div styleName='doctor-second-fold'>
            <div styleName='second-fold-block'>
              <div styleName='second-fold-block-title'>Experiences</div>
              <div>{this.experienceList()}</div>
            </div>
            <hr styleName='hr-color'/>
            <div styleName='second-fold-block'>
              <div styleName='second-fold-block-title'>Education</div>
              <div>{this.educationList()}</div>
            </div>
          </div>
        </div>
      )
  },
  render() {
    return (
      <div styleName='doctor-profile'>
        {this.renderContent()}
      </div>
    )
  }
})

export default CSSModules(DoctorProfile, styles);
