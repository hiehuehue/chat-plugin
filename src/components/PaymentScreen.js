import React from 'react';
import styles from 'styles/PaymentScreen.scss';
import CSSModules from 'react-css-modules';
import Paper from 'material-ui/Paper';
import PromoSvg from 'images/promo.svg';
import TextField from 'material-ui/TextField';
const PaymentScreen = React.createClass({
  changePromocode(val) {
    var isValid = true;
    if (isValid) {
      this.props.changePromocode(val);
    }
  },
  applyPromocode: function () {
    // Add email
    const {queryId, promoCode, chatPassword} = this.props;
    this.props.getPromocodeDetails({queryId, promoCode, chatPassword});
  },
  consultationFees: function () {
    return this.props.queryPaymentDetails.consultation_fees
  },
  internetFees: function () {
    return this.props.queryPaymentDetails.internet_fees
  },
  serviceTax: function () {
    return this.props.queryPaymentDetails.service_tax
  },
  swatchBharatFees: function () {
    return this.props.queryPaymentDetails.swatch_bharat_fees
  },
  krishiKalyanFees: function () {
    return this.props.queryPaymentDetails.krishi_kalyan_fees
  },
  totalFees: function () {
    return Number(this.props.queryPaymentDetails.total_fees) - this.discount()
  },
  discount: function () {
    return Number(this.props.promoCodeDetails.discount || 0)
  },
  componentWillMount() {
    const {queryId, chatPassword} = this.props;
    this.props.getQueryPaymentDetails({queryId, chatPassword});
  },
  cancelPromoCode(){
    this.props.savePromocodeDetails({});
    this.props.changePromocode('');
  },
  render() {
    const promoCodeField = Object.keys(this.props.promoCodeDetails).length === 0
      ? <div styleName='promo-code-wrapper'>
          <PromoSvg styleName='promo-code-svg'/>
          <TextField styleName='promo-text-field' hintText="Promocode" fullWidth={true} value={this.props.promoCode} onChange={(e) => this.changePromocode(e.target.value)}/>
          <button styleName='promo-apply-button' onTouchTap={this.applyPromocode}>APPLY</button>
        </div>
      : <div styleName={this.props.promoCodeDetails.success ? 'promo-successful' : 'promo-unsuccessful'}><div styleName='promo-successful-message'>{this.props.promoCodeDetails.message}</div><div styleName='promo-apply-button' onTouchTap={this.cancelPromoCode}>CLEAR</div></div>;
    return (
      <div styleName='payment-screen'>
        <Paper styleName='promo-code-paper' zDepth={1}>
          <div styleName='promo-code-text'>Apply Promocode</div>
          <div styleName='promo-code-card'>
            {promoCodeField}
          </div>
        </Paper>
        <Paper styleName='details-paper' zDepth={1}>
          <div styleName='details-title'>
            Payment Details
          </div>
          <div styleName='details-container'>
            <div  styleName='details-block'>
              <div styleName='details-left'>
                <div styleName='details-left-title'>ITEM NAME</div>
                <div>Consultation Fees
                </div>
                <div>Internet Fees
                </div>
                <div>Service Tax
                </div>
                <div>Swatch Bharat Fees
                </div>
                <div>Krishi Kalyan Fees
                </div>
                <div>Discount
                </div>
              </div>
              <div styleName='details-right'>
                <div styleName='details-right-title'>PRICE ₹</div>
                <div>{this.consultationFees()}</div>
                <div>{this.internetFees()}</div>
                <div>{this.serviceTax()}</div>
                <div>{this.swatchBharatFees()}</div>
                <div>{this.krishiKalyanFees()}</div>
                <div>{this.discount()}</div>
              </div>
            </div>
            <hr styleName='hr-color'/>
            <div styleName='details-block'>
              <div styleName='details-left'>
                <div styleName='details-text'>Total</div>
              </div>
              <div styleName="details-right">
                <div styleName='details-text'>{this.totalFees()}</div>
              </div>
            </div>
          </div>
        </Paper>
      </div>
    )
  }
})
export default CSSModules(PaymentScreen, styles);
