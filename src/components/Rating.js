import React from 'react'
import CSSModules from 'react-css-modules';
import styles from 'styles/Rating.scss';
import * as CONSTANTS from 'constants';

const Rating = React.createClass({
  renderRating() {
    const {rating, responder, changeRating, bottomActionButton} = this.props;
    const ratingStars = Array(rating).fill('☆');
    const ifBot = responder === CONSTANTS.SUPPORT_BOT_NAME;
    const unFilledStars = ifBot
      ? Array(3 - rating).fill('')
      : Array(5 - rating).fill('');
    const allStars = ratingStars.concat(unFilledStars);
    const isSubmitted = bottomActionButton === CONSTANTS.POST_NEW_QUERY;
    const stars = allStars.map((star, i) => {
      if (star) {
        return (
          <span key={i} styleName={isSubmitted?'with-stars-no-click':'with-stars'} onTouchTap={() => changeRating(i + 1)}></span>
        )
      } else {
        return (
          <span key={i} onTouchTap={() => changeRating(i + 1)}>☆</span>
        )
      }
    });
    return (
      <div styleName='rating'>{stars}</div>
    )
  },
  render() {
    const {responder, rating, chatPassword, queryId, submitRating} = this.props;
    const isDisabled = rating === 0;
    return (
      <div>
        {this.renderRating()}
        {this.props.bottomActionButton === CONSTANTS.POST_NEW_QUERY
          ? null
          : (
            <button styleName='submit-rating-button' disabled={isDisabled} onTouchTap={() => submitRating({responder, rating, queryId, chatPassword})}>
              SUBMIT
            </button>
          )}
      </div>
    )
  }
})

export default CSSModules(Rating, styles);
