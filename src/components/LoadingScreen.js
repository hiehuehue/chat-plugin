import React from 'react'
import styles from 'styles/LoadingScreen.scss';
import CSSModules from 'react-css-modules';
import SpinnerSvg from 'images/spinner.svg';
const LoadingScreen = React.createClass({
  componentDidMount() {
    this.props.changeLoadingScreenVisibility(false);
  },
  render () {
    return (
      <div styleName={this.props.loadingScreenVisibility ? 'loading-screen-visible' : 'loading-screen-invisible'}>
        <SpinnerSvg styleName='spinner-svg'/>
      </div>
    )
  }
})

export default CSSModules(LoadingScreen, styles);
