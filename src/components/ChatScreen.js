import React from 'react';
import CSSModules from 'react-css-modules';
import styles from 'styles/ChatScreen.scss';
import MessageContainerWrapper from 'wrappers/MessageContainerWrapper';
import PoweredBy from 'images/powered.png';
import {getDomain} from 'helpers/parsers';
const ChatScreen = React.createClass({
  componentWillMount() {
    const { props: { jidAndPassword : {jid, chatPassword}, queryId } } = this;
    this.props.initiateXmppConnection({jid, chatPassword});
    this.props.watchOnMessage(queryId);
    this.props.watchOnRecipientAck(queryId);
    this.props.watchOnServerAck(queryId);
    this.props.syncChatHistory({queryId, chatPassword, time:new Date().toISOString()})
    if(this.props.hour24TimerVisibility){
      this.props.continue24HourTimer();
    }
    this.props.watchOnReconnection(this.props.sendUnSentMessages);
  },
  componentWillUnmount() {
    this.props.watchOnMessage('');
    this.props.watchOnRecipientAck('');
    this.props.watchOnServerAck('');
    this.props.watchOnReconnection('');
    this.props.closeXmppConnection();
  },
  scrollToBottom() {
    const scrollHeight = this.messageList.scrollHeight;
    const height = this.messageList.clientHeight;
    const maxScrollTop = scrollHeight - height;
    this.messageList.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
  },
  componentDidMount() {
    setTimeout(() => {
      this.scrollToBottom();
    });
  },
  componentDidUpdate() {
    this.scrollToBottom();
  },
  renderMessages(){
    return this.props.allMessages.map(function (msg, i, arr) {
      if(i === 0){
        return <MessageContainerWrapper key={i} msg={msg}/>;
      } else {
        var newMsg = {...msg, upResponder: arr[i -1].responder};
        return <MessageContainerWrapper key={i} msg={newMsg}/>;
      }
    });
  },
  render () {
    const that = this;
    const domain = getDomain();
    const isPSTakeCare = domain === 'pstakecare';
    const poweredByImage = !isPSTakeCare ? <img styleName='powered-by' src={PoweredBy} alt=""/>:''
    return (
      <div styleName='chat-screen'>
        <div styleName='message-container' ref={(div) => {that.messageList = div}}>
          {this.renderMessages()}
        </div>
        <div styleName='powered-by-container'>
          {poweredByImage}
        </div>
      </div>
    )
  }
})

export default CSSModules(ChatScreen, styles);
