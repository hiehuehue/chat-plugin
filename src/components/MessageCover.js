import React from 'react';
import CSSModules from 'react-css-modules';
import styles from 'styles/MessageCover.scss';
import {
  timeAgo
} from 'helpers/timestamp';
import DoubleTickSvg from 'images/double-tick.svg';
import SingleTickSvg from 'images/single-tick.svg';
const MessageCover = React.createClass({
  render () {
    const {msg, children, msg: {upResponder, status, responder}, name} = this.props;
    const isRight = this.props.msg.to === this.props.jid;
    const showName = !upResponder || (upResponder !== responder && responder !== name);
    const showHangingArrow = !upResponder || (upResponder !== responder);
    const nameTop = msg.responder === 'support' ? 'PSTakeCare' : msg.responder;
    return (
      <div styleName={isRight ? showHangingArrow ? 'message-cover-right' : 'message-cover-right-no-pad' : showHangingArrow ? 'message-cover-left' : 'message-cover-left-no-pad'}>
        {isRight?<div styleName = {showHangingArrow ? 'message-hanging-right-arrow': 'message-hanging-right-arrow-hidden'}></div>:null}
        <div styleName='message-box'>
          {showName?<div styleName='message-name'>
            {nameTop}
          </div>:null}
          <div styleName='message-children'>
            {children[0]}
          </div>
          <div styleName='bottom-part'>
            <div>
              {timeAgo(msg.timestamp)}
            </div>
            {!isRight? <div styleName = 'tick-container'>
              {status === 2 ? <DoubleTickSvg  styleName='tick-color'/> : status === 1 ? <SingleTickSvg styleName='tick-color'/> : null}
            </div>: null}
          </div>
          {children[1]}
        </div>
        {!isRight?<div styleName = {showHangingArrow ?'message-hanging-left-arrow':'message-hanging-left-arrow-hidden'}></div>:null}
      </div>
    )
  }
});

export default CSSModules(MessageCover, styles,{'allowMultiple':true});
