import React from 'react'
import * as CONSTANTS from 'constants';
import CSSModules from 'react-css-modules';
import styles from 'styles/SendMessageBar.scss';
import AttachmentSvg from '../images/attachment.svg';
import SendSvg from '../images/send-icon.svg';
const SendMessageBar = React.createClass({
  sendTextMessage() {
    if (this.props.message) {
      const {
        props: {
          message,
          receiverJidAndName: {
            jid: receiverJid
          },
          queryId,
          name
        }
      } = this;
      this.props.sendAndStoreMessage({
        to: receiverJid,
        body: {
          msg_type: CONSTANTS.MSG,
          query_id: queryId,
          body: message
        },
        timestamp: new Date().getTime(),
        status: 0,
        responder: name
      });
      this.props.changeMessageValue('');
    }
  },
  changeMessageValue(e) {
    // FIXME: validation
    var isValid = true;
    if (isValid) {
      this.props.changeMessageValue(e.target.value);
    }
  },
  sendTextMessageOnEnter(e) {
    if (e.keyCode == 13) {
      this.sendTextMessage();
    }
  },
  sendFile(e) {
    e.preventDefault();
    const {
      props: {
        receiverJidAndName: {
          jid: receiverJid
        },
        queryId,
        name
      }
    } = this;
    Array.prototype.forEach.call(e.target.files, (file) => {
      var isDocument = file.type === 'application/pdf';
      var isImage = file.type === 'image/jpg' || file.type === 'image/png' || file.type === 'image/jpeg';
      if (isDocument || isImage) {
        this.props.uploadAndSendFile({
          to: receiverJid,
          body: {
            msg_type: isImage?CONSTANTS.IMAGE:isDocument?CONSTANTS.DOCUMENT:'',
            query_id: queryId
          },
          timestamp: new Date().getTime(),
          status: 0,
          responder: name,
          file: file
        });
      } else {
        // FIXME show unsent files to user
        console.log(file.type + ' this file type is unsupported');
        this.props.pushError({
          type: CONSTANTS.RETRY,
          e: new Error('unsupported file type'),
          warnLevel: CONSTANTS.WARN_LEVEL_RED,
          status: file.type + ' this file type is unsupported'
        })
      }
    });
    e.target.value = '';
    return false;
  },
  render() {
    return (
      <div styleName='send-message-bar'>
        <div styleName='image-upload'>
          <label htmlFor='file'>
            <AttachmentSvg styleName='attachement-svg'/>
          </label>
          <input type='file' id='file' accept="application/pdf,image/png,image/jpg,image/jpeg" capture="camera" multiple onChange={this.sendFile}/>
        </div>
        <label styleName='send-message-label'>
          <input id="PSTC-Chat-Send" styleName='send-message' value={this.props.message} onChange={this.changeMessageValue} type='text' placeholder='Type your message' onKeyDown={this.sendTextMessageOnEnter}/>
          <div styleName='send-icon-container' onTouchTap={this.sendTextMessage}>
            <SendSvg styleName={this.props.message ? 'send-icon-active' : 'send-icon'}/>
          </div>
        </label>
      </div>
    )
  }
});


export default CSSModules(SendMessageBar, styles);
