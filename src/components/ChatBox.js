import React from 'react';
import MainContainerWrapper from 'wrappers/MainContainerWrapper';
import ErrorComponentWrapper from 'wrappers/ErrorComponentWrapper';
import SendMessageBarWrapper from 'wrappers/SendMessageBarWrapper';
import * as CONSTANTS from 'constants';
import CSSModules from 'react-css-modules';
import styles from 'styles/ChatBox.scss';
import {validatePhoneNo, validateName} from 'helpers/validators';

const ChatBox = React.createClass({
  initiatePayment() {
    const {
      queryId,
      jidAndPassword: {
        chatPassword
      },
      phoneNo,
      name,
      email,
      promoCodeDetails,
      queryPaymentDetails
    } = this.props;
    this.props.submitPaymentDetails({
      queryId,
      chatPassword,
      phoneNo,
      name,
      email,
      promoCodeDetails,
      queryPaymentDetails
    });
  },
  submitFirstScreenForm(){
    var {name, phoneNo, queryBody} = this.props;
    if(name.trim().length !== 0 && queryBody.trim().length !== 0 && phoneNo.trim().length === 10){
      this.props.submitFirstScreenForm({name, phoneNo, queryBody});
      this.props.sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.CLICK_NEXT, CONSTANTS.FIRST_QUERY_INITIATION);
    } else {
      validateName(name, (err)=>{
        if(err) this.props.changeErrorMessageForName(err);
      });
      validatePhoneNo(phoneNo, (err)=>{
        if(err) this.props.changeErrorMessageForPhoneNo(err);
      });
      if(queryBody.trim().length === 0){
        this.props.changeErrorForNewQueryBody('Please write your Query');
      }
    }
  },
  componentDidMount() {
    this.props.changePaymentSubmitAbility(true);
  },
  confirmOTP() {
    // FIXME: validation
    var isValid = true;
    if (isValid) {
      const {
        phoneNo,
        otp,
        userVerificationStatus,
        authenticationToken,
        queryBody,
        name
      } = this.props
      this.props.confirmOTP({
        phoneNo,
        otp,
        userVerificationStatus,
        authenticationToken,
        queryBody,
        name
      });
    }
  },
  submitQueryAgain(){
    // FIXME: validation
    var {jidAndPassword:{jid, chatPassword}, name, queryBody, phoneNo} = this.props;
    const isValid = queryBody.trim().length !== 0 && jid.length !== 0 && chatPassword.length !== 0;
    if(queryBody.trim().length === 0){
      this.props.changeErrorForNewQueryBody('Please write your Query');
    }
    if(isValid){
      this.props.sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.CLICK_RESUBMIT, CONSTANTS.REPEAT_QUERY_SUBMIT);
      this.props.submitQueryAgain({jidAndPassword:{jid,chatPassword}, name, queryBody, phoneNo});
    }
  },
  openNewQuery(){
    this.props.openNewQuery();
    this.props.sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.CLICK_POSTNEWQUERY, CONSTANTS.SECOND_QUERY_STARTED);
  },
  renderBottomActionButton(){
    const {
      bottomActionButton,
      submitAbility,
      otpSubmitAbility,
      queryReSubmitAbility,
      errorForName,
      errorForPhoneNo,
      queryBody,
      otp,
      otpErrorMessage,
      paymentSubmitAbility,
      phoneNo,
      name
    } = this.props;
    const isValidSubmission = name.trim().length !== 0 && queryBody.trim().length !== 0 && phoneNo.trim().length === 10;
    const isFirstScreenNextDisabled = (!!errorForName || !!errorForPhoneNo || !isValidSubmission);
    const isOtpScreenDisabled = otp.trim().length === 0 || otpErrorMessage;
    switch (bottomActionButton) {
      case CONSTANTS.FIRST_SCREEN_NEXT:
        return (<button styleName={(isFirstScreenNextDisabled || !submitAbility) ? 'first-screen-next-inactive':'first-screen-next-active'} type='button' onTouchTap={this.submitFirstScreenForm} disabled={isFirstScreenNextDisabled || !submitAbility}>NEXT</button>)
      case CONSTANTS.SUBMIT_OTP_AND_QUERY:
        return (<button styleName={(isOtpScreenDisabled || !otpSubmitAbility) ? 'first-screen-next-inactive':'first-screen-next-active'} type='button' onTouchTap={this.confirmOTP} disabled={isOtpScreenDisabled || !otpSubmitAbility}>SUBMIT</button>)
      case CONSTANTS.SEND_CHAT_MESSAGE:
      return <SendMessageBarWrapper/>
      case CONSTANTS.CONTINUE_TO_PAYMENT:
        return (!this.props.paymentCompleteStatus?<button styleName={paymentSubmitAbility ? 'payment-submit-button-active': 'payment-submit-button-inactive'} onTouchTap={() => this.initiatePayment()} disabled={!paymentSubmitAbility}>
          CONTINUE
        </button>:null);
      case CONSTANTS.POST_NEW_QUERY:
        return (<button styleName='post-new-query-button' onTouchTap={this.openNewQuery}>POST NEW QUERY</button>)
      case CONSTANTS.SUMBIT_NEW_QUERY:
        return (<button styleName='submit-bottom-button' type='button' onTouchTap={this.submitQueryAgain} disabled={queryReSubmitAbility}>SUBMIT</button>)
      default:
        return null;
    }
  },
  onChatBoxClick() {
    if (this.props.unreadMessageCount) {
      this.props.clearUnreadMessageCount();
    }
  },
  render () {
    return (
      <div onTouchTap={this.onChatBoxClick} styleName={this.props.visibility ? 'chat-box-style-visible': 'chat-box-style-invisible'}>
        {this.props.errors?<ErrorComponentWrapper/>:null}
        <MainContainerWrapper/>
        {this.renderBottomActionButton()}
      </div>
    )
  }
})


export default CSSModules(ChatBox, styles, {'allowMultiple':true});
