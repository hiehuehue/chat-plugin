import React from 'react';
import TextField from 'material-ui/TextField';
import CSSModules from 'react-css-modules';
import styles from 'styles/QueryNamePhoneForm.scss';
import {validatePhoneNo, validateName} from 'helpers/validators';

const QueryNamePhoneForm = React.createClass({
  changeName(e){
    validateName(e.target.value, (err)=>{
      if(err){
        this.props.changeErrorMessageForName(err);
      }else {
        this.props.changeErrorMessageForName('');
      }
      this.props.changeName(e.target.value);
    })
  },
  changePhoneNo(e){
    validatePhoneNo(e.target.value, (err)=>{
      if(err){
        this.props.changeErrorMessageForPhoneNo(err);
      } else {
        this.props.changeErrorMessageForPhoneNo('');
      }
      this.props.changePhoneNo(e.target.value);
    });
  },
  onPhoneBlur(e){
    this.changePhoneNo(e);
    if(e.target.value.length < 10){
      this.props.changeErrorMessageForPhoneNo('Please use a 10 digit phone number');
    }
  },
  changeQueryBody(e){
    if(this.props.errorForNewQueryBody){
      this.props.changeErrorForNewQueryBody('');
    }
    this.props.changeQueryBody(e.target.value);
  },
  componentDidMount() {
    if(this.props.name.length > 0 && this.props.queryBody.length > 0){
      setTimeout(()=>{
        this.refs.phoneInputField.focus();
      }, 100);
    }
    this.props.changeSubmitButtonAbility(true);
  },
  render () {
    return (
      <div styleName='query-name-phone-form'>
        <TextField styleName='patient-name' errorText={this.props.errorForName} floatingLabelText="Patient Name" fullWidth={true} value={this.props.name} onBlur={()=>this.changeName({target:{value:this.props.name}})} onChange={this.changeName}/>
        <TextField styleName='patient-phoneNo' errorText={this.props.errorForPhoneNo} floatingLabelText="Phone Number" fullWidth={true} value={this.props.phoneNo} ref='phoneInputField' onBlur={()=>this.onPhoneBlur({target:{value:this.props.phoneNo}})} onChange={this.changePhoneNo}/>
        <TextField styleName='query-body-name' rows={3} rowsMax={8} multiLine={true} errorText={this.props.errorForNewQueryBody} hintText="Please specify your concerns. Include all symptoms and how long you've had them." value={this.props.queryBody} fullWidth={true} onChange={this.changeQueryBody}/>
      </div>
    )
  }
})

export default CSSModules(QueryNamePhoneForm, styles);
