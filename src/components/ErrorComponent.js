import React from 'react'
import styles from 'styles/ErrorComponent.scss';
import CSSModules from 'react-css-modules';
import CloseSvg from 'images/close.svg';
const ErrorComponent = React.createClass({
  render () {
    return (
      <div styleName='error-component'>
        <div>{this.props.errors.status}</div>
        <CloseSvg styleName='close-svg' onTouchTap={this.props.closeErrorScreen}/>
      </div>
    )
  }
})

export default CSSModules(ErrorComponent, styles);
