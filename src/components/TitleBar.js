import React from 'react';
import CSSModules from 'react-css-modules';
import styles from 'styles/TitleBar.scss';
import * as CONSTANTS from 'constants';
import GoBackSvg from 'images/go-back.svg';
import MaximiseSvg from 'images/maximise.svg';
import MinimiseSvg from 'images/minimise.svg';
const TitleBar = React.createClass({
  render24hrTime() {
    return (this.props.hour24TimerVisibility
      ? <div styleName='hour-24-timer'>{this.props.hour24Timer}</div>
      : null)
  },
  renderCloseButton() {
    switch (this.props.popUpScreenName) {
      case CONSTANTS.PAYMENT_SCREEN:
        return <GoBackSvg styleName='go-back' onTouchTap={() => {
          this.props.popUpScreen('');
          this.props.savePromocodeDetails({});
          this.props.changePromocode('');
          this.props.changeBottomActionButton(CONSTANTS.SEND_CHAT_MESSAGE);
        }}/>
      case CONSTANTS.DOCTOR_PROFILE_SCREEN:
        return <GoBackSvg styleName='go-back' onTouchTap={() => {
          this.props.popUpScreen('');
          this.props.changeBottomActionButton(CONSTANTS.SEND_CHAT_MESSAGE);
        }}/>
      default:
        null;
    }
  },
  onTitleBarClick() {
    const {
      unreadMessageCount, clearUnreadMessageCount, changeVisibilityOfChat, isUnInitiated, screenState, visibility, changeToInitiated, sendGA
    } = this.props;
    if (unreadMessageCount) {
      clearUnreadMessageCount();
    }
    changeVisibilityOfChat();
    if (isUnInitiated && screenState === CONSTANTS.GET_QUERY_PHONE_NAME_SCREEN && !visibility){
      changeToInitiated();
      sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.CLICK_CONSULTWITHDOCTOR, CONSTANTS.FIRST_INTERACTION);
    }
  },
  componentDidMount() {
    const {isUnInitiated, screenState, sendGA} = this.props;
    if(isUnInitiated && screenState === CONSTANTS.GET_QUERY_PHONE_NAME_SCREEN){
      setTimeout(function () {
        sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.LOAD_INITIALSTATE, CONSTANTS.INITIAL_STATE_VISITORS)
      }, 10);
    }
  },
  render() {
    const {
      unreadMessageCount,
      visibility,
      popUpScreenName,
      title,
      doctorProfile: {
        name,
        prefix
      },
      isFirstUser
    } = this.props;
    const titleContent = popUpScreenName === CONSTANTS.DOCTOR_PROFILE_SCREEN
      ? null
      : title;
    const titleVal = titleContent
      ? titleContent
      : prefix + ' ' + name
    const showTitle = !visibility || titleContent;
    const colorOfTitleBarAfterOnVisit =  'title-bar-shadow-color';
    const colorOfTitleBarAfterFirstVisit = !unreadMessageCount && !titleContent
      ? 'title-bar-no-shadow'
      : !unreadMessageCount && titleContent
        ? 'title-bar-shadow'
        : unreadMessageCount && !titleContent
          ? 'title-bar-no-shadow-color'
          : 'title-bar-shadow-color';
    const colorOfTitleBar = (!visibility && isFirstUser) ? colorOfTitleBarAfterOnVisit : colorOfTitleBarAfterFirstVisit;
    return (
      <div styleName={colorOfTitleBar}>
        {this.renderCloseButton()}
        <div onTouchTap={this.onTitleBarClick} styleName='title-bar-block'>
          <div styleName='title-block'>
            {showTitle
              ? <div>
                  <div styleName='title-content'>{titleVal}</div>
                  {this.render24hrTime()}
                </div>
              : null}
          </div>
          {unreadMessageCount
            ? <div styleName='msg-unread-count'>{unreadMessageCount}</div>
            : null}
          {visibility
            ? <MinimiseSvg styleName='dash-mise'/>
            : <MaximiseSvg styleName='dash-mise'/>}
        </div>
      </div>
    )
  }
});

export default CSSModules(TitleBar, styles);
