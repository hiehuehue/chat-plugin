import React from 'react';
import QueryNamePhoneFormWrapper from 'wrappers/QueryNamePhoneFormWrapper';
import OtpVerificationWrapper from 'wrappers/OtpVerificationWrapper';
import ChatScreenWrapper from 'wrappers/ChatScreenWrapper';
import NewQueryScreenWrapper from 'wrappers/NewQueryScreenWrapper';
import DoctorProfileWrapper from 'wrappers/DoctorProfileWrapper';
import PaymentScreenWrapper from 'wrappers/PaymentScreenWrapper';
import * as CONSTANTS from 'constants';

const MainContainer = React.createClass({
  renderContent(){
    switch (this.props.screenState) {
      case CONSTANTS.GET_QUERY_PHONE_NAME_SCREEN:
        return <QueryNamePhoneFormWrapper/>
      case CONSTANTS.OTP_VERIFICATION_SCREEN:
        return <OtpVerificationWrapper/>
      case CONSTANTS.CHAT_SCREEN:
        return <ChatScreenWrapper/>
      case CONSTANTS.NEW_QUERY_SCREEN:
        return <NewQueryScreenWrapper/>
      default:
        return <div>default</div>
    }
  },
  render() {
    const {popUpScreenName} = this.props;
    return (
      <div style={mainContainerStyle}>
        {popUpScreenName === CONSTANTS.DOCTOR_PROFILE_SCREEN?<DoctorProfileWrapper/>:popUpScreenName === CONSTANTS.PAYMENT_SCREEN?<PaymentScreenWrapper/>:null}
        {this.renderContent()}
      </div>
    )
  }
});

const mainContainerStyle = {
  height: 'calc(100% - 50px)',
  display: 'flex',
  overflow: 'auto',
  borderBottom: '1px solid #ddd',
  position: 'relative'
}

export default MainContainer;
