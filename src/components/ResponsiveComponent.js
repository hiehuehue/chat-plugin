import React from 'react'
import {
  BREAKPOINT_MOBILE
} from 'constants';
const ResponsiveComponent = React.createClass({
  getInitialState() {
    return {
      orientation: 'desktop'
    }
  },
  _updateDimensions(e) {
    if (e.target.innerWidth > BREAKPOINT_MOBILE) {
      this.setState({
        orientation: 'desktop'
      });
    } else {
      this.setState({
        orientation: 'mobile'
      });
    }
  },
  componentWillMount() {
    this._updateDimensions({
      target: {
        innerWidth: (window.innerWidth > 0) ? window.innerWidth : screen.width
      }
    });
  },
  componentDidMount() {
    window.addEventListener('resize', this._updateDimensions);
  },
  componentWillUnmount() {
    window.removeEventListener('resize', this._updateDimensions);
  },
  render() {
    return (React.cloneElement(this.props.children, {
      orientation: this.state.orientation
    }))
  }
})

export default ResponsiveComponent;
