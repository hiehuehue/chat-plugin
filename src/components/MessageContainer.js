import React from 'react';
import * as CONSTANTS from 'constants';
import CSSModules from 'react-css-modules';
import RatingWrapper from 'wrappers/RatingWrapper';
import styles from 'styles/MessageContainer.scss';
import MessageCoverWrapper from 'wrappers/MessageCoverWrapper';
import {getFileNameFromFileUrl} from 'helpers/parsers.js';
import PDFSvg from 'images/pdf.svg';
const MessageContainer = React.createClass({
  renderMessage() {
    const {msg} = this.props;
    const ifBot = msg.responder === CONSTANTS.SUPPORT_BOT_TITLE;
    switch (msg.msgType) {
      case CONSTANTS.MSG:
        return <div>{msg.body}</div>
      case CONSTANTS.IMAGE:
        return <a href={msg.body} target='blank'><img styleName='message-image' src={msg.body} alt=""/></a>
      case CONSTANTS.DOCUMENT:
        return <div>
          <a styleName='pdf-message' href={msg.body} target='blank'><PDFSvg/><div styleName='pdf-name'>{getFileNameFromFileUrl(msg.body)}</div>
          </a>
        </div>
      case CONSTANTS.QUERY_END:
        return (
          <div>
            {ifBot
              ? <div>We hope your query was satifactorily resolved. How would you rate our support agent?</div>
              : <div>We hope your query was satifactorily resolved. How would you rate your conversation with {msg.doctorName}?</div>}
          </div>
        )
      case CONSTANTS.PICKED_BY_DOCTOR:
        return (
          <div>
            <button styleName='picked-by-doctor'>
              {msg.name}&nbsp; is now available to answer your queries. Please pay consulation fees of ₹{msg.consultation_cost}
            </button>
          </div>
        )
      default:
        console.log(msg);
        return null;
    }
  },
  renderSpecialMessages(specialCase) {
    if (specialCase.showQueryEndByDoctor) {
      return (
        <div styleName='special-message'>Your consulation session is now complete</div>
      )
    } else if (specialCase.showPaymentReceivedWithExpiry) {
      return (
        <div>
          <div styleName='special-message'>
            Payment received
          </div>
          <div styleName='special-message'>
            Your chat will expire in 24 hrs
          </div>
        </div>
      );
    } else if (specialCase.showOnlyExpiry) {
      return (
        <div styleName='special-message'>
          Your chat will expire in 24 hrs
        </div>
      )
    }
  },
  renderBelowTimeMessage() {
    switch (this.props.msg.msgType) {
      case CONSTANTS.QUERY_END:
        return <div styleName='pay-now-wrapper'><RatingWrapper/></div>
      case CONSTANTS.PICKED_BY_DOCTOR:
        return <div styleName='pay-now-wrapper'>
          <button styleName='pay-now-button' onTouchTap={() => {
            this.props.popUpScreen(CONSTANTS.DOCTOR_PROFILE_SCREEN);
            this.props.changeBottomActionButton('');
            this.props.sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.CLICK_VIEWDOCTORPROFILE, CONSTANTS.DOCTOR_PROFILE_VIEWED);
          }}>
            VIEW DOCTOR PROFILE
          </button>
          {!this.props.paymentCompleteStatus?<button styleName='pay-now-button' onTouchTap={() => {
            this.props.popUpScreen(CONSTANTS.PAYMENT_SCREEN);
            this.props.changeBottomActionButton(CONSTANTS.CONTINUE_TO_PAYMENT);
            this.props.sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.CLICK_PAYNOW, CONSTANTS.PAYMENT_PROCESS_STARTED);
          }}>
            PAY NOW
          </button>:null}
        </div>
      default:
        return null;
    }
  },
  render() {
    const {
      msg: {
        msgType,
        consultation_cost
      },
      responder
    } = this.props;
    const showPaymentReceivedWithExpiry = (msgType === CONSTANTS.PAYMENT_COMPLETED && Number(consultation_cost));
    const showOnlyExpiry = (msgType === CONSTANTS.PAYMENT_COMPLETED && !Number(consultation_cost));
    const showQueryEnd = msgType === CONSTANTS.QUERY_END;
    const isSpecial = showQueryEnd || showPaymentReceivedWithExpiry || showOnlyExpiry;
    const showQueryEndByDoctor = msgType === CONSTANTS.QUERY_END && responder !== CONSTANTS.SUPPORT_BOT_NAME;
    const specialCase = {
      showQueryEndByDoctor,
      showPaymentReceivedWithExpiry,
      showOnlyExpiry
    };
    const isNormal = msgType !== CONSTANTS.PAYMENT_COMPLETED && msgType !== CONSTANTS.PICKED_BY_AGENT;
    const isBelowTime = msgType === CONSTANTS.QUERY_END || msgType === CONSTANTS.PICKED_BY_DOCTOR;
    return (
      <div>
        {isSpecial
          ? <div>{this.renderSpecialMessages(specialCase)}</div>
          : null}
        {isNormal
          ? <MessageCoverWrapper msg={this.props.msg}>
              {this.renderMessage()}
              {isBelowTime
                ? <div>{this.renderBelowTimeMessage()}</div>
                : null}
            </MessageCoverWrapper>
          : null}
      </div>
    )
  }
});

export default CSSModules(MessageContainer, styles);
