import React from 'react';
import TextField from 'material-ui/TextField';
import styles from 'styles/NewQueryScreen.scss';
import CSSModules from 'react-css-modules';

const NewQueryScreen = React.createClass({
  changeQueryBody(e){
    // FIXME: validation
    var isValid = true;
    if(isValid){
      if(this.props.queryBody.length === 0){
        this.props.changeErrorForNewQueryBody('');
      }
      this.props.changeQueryBody(e.target.value);
    }
  },
  componentDidMount() {
    this.props.changeQueryReSubmitAbility(false);
  },
  render () {
    return (
      <div styleName='new-query-screen'>
        <TextField styleName='query-body-name' rows={3} rowsMax={8} errorText={this.props.errorForNewQueryBody} multiLine={true} fullWidth={true} hintText="Please specify your concerns include all symptoms and how long you've had them" value={this.props.queryBody} fullWidth={true} onChange={this.changeQueryBody}/>
      </div>
    )
  }
})

export default CSSModules(NewQueryScreen, styles);
