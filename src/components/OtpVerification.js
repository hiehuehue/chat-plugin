import React from 'react';
import TextField from 'material-ui/TextField';
import CSSModules from 'react-css-modules';
import styles from 'styles/OtpVerification.scss';
import * as CONSTANTS from 'constants';
const OtpVerification = React.createClass({
  changeOTPValue(e) {
    this.props.changeOtpErrorMessage('');
    this.props.changeOTPValue(e.target.value);
  },
  resendOTP() {
    const {phoneNo, otp, userVerificationStatus, authenticationToken} = this.props
    this.props.resendOTP({phoneNo, otp, userVerificationStatus, authenticationToken});
    this.props.startOTPTimer();
    this.props.sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.CLICK_RESEND, CONSTANTS.OTP_RESENT)
  },
  componentDidMount() {
    this.props.startOTPTimer();
    this.props.otpSubmitAbility(true);
  },
  componentWillUnmount() {
    this.props.changeOTPValue('');
  },
  changeNumber(){
    this.props.changeNumber();
    this.props.sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.CLICK_EDITNUMBER, CONSTANTS.OTP_EDIT)
  },
  render() {
    let timeLeft = (this.props.resendTick >= 60
      ? '01'
      : '00') + ':' + (this.props.resendTick % 60 > 9
      ? (this.props.resendTick % 60)
      : ('0' + this.props.resendTick % 60));
    let resendOTPLabel = this.props.resendButtonVisibility
      ? <button styleName='submit-button' type='button' onTouchTap={this.resendOTP} disabled={!this.props.otpResendAbility}>RESEND OTP</button>
      : <div>Didn't receive a code<br/>Please wait&nbsp;<span styleName='time-left'>{timeLeft}</span>&nbsp;</div>;
    return (
      <div styleName='otp-verification'>
        <div styleName='otp-text'>Verification code sent to {this.props.phoneNo}</div>
        <TextField styleName='otp-field' floatingLabelText="Enter OTP" errorText={this.props.otpErrorMessage} onChange={this.changeOTPValue} value={this.props.otp}/>
        <div styleName='button-wrapper'>
          {resendOTPLabel}
          <button type='button' styleName='submit-button' onTouchTap={this.changeNumber}>EDIT NUMBER</button>
        </div>
      </div>
    )
  }
})

export default CSSModules(OtpVerification, styles);
