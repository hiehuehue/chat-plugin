export function validatePhoneNo(number, callback) {
  var reg = /^\d*$/;
  if (number.length > 10) {
    return callback('Please keep your number under 10 digits');
  } else if (!reg.test(number)) {
    return callback('Please use only digits');
  } else if (number.length === 0) {
    return callback('Please write your Phone Number');
  } else {
    return callback(null);
  }
}
export function validateName(val, callback) {
  var reg = /[a-zA-Z ]*/;
  if (!reg.test(val)) {
    return callback('Please use only letters to form a name', null);
  } else if (val.trim().length === 0) {
    return callback('Please write your Name', null);
  } else {
    return callback(null);
  }
}
