import PstcChat from 'modular-chat';
import {
  eventChannel
} from 'redux-saga'

var connectionObjectPromise = null;
var connectionObject = null;

function connectPstcChat(args) {
  try {
    connectionObjectPromise = null;
    connectionObject = null;
    connectionObjectPromise = PstcChat(args);
  } catch (e) {
    console.log(e);
    setTimeout(function () {
      connectionObjectPromise = null;
      connectionObject = null;
      connectPstcChat(args);
      connectionObjectPromise.then(connection => {
        connectionObject = connection
      });
    }, 2000);
  }
}
export function xmppConnectionInit(args) {
  connectPstcChat(args);
  connectionObjectPromise.then(connection => {
    connectionObject = connection
  });
  return connectionObjectPromise;
}

export function xmppConnectionClose() {
  connectionObject.disconnect();
  connectionObjectPromise = null;
  connectionObject = null;
}

export function xmppConnectionObject() {
  return connectionObjectPromise;
}

export function sendTextMessage(to, body) {
  return new Promise(function (resolve, reject) {
    connectionObjectPromise.then(connection => {
      connectionObject = connection
      resolve(connectionObject.sendMessage(to, body));
    }).catch((err) => {
      reject(err);
    });
  });
}

export function addReconnectionCallback(connection, callback) {
  if (callback) {
    connection.onReconnection(callback);
  } else {
    connection.removeOnReconnection(callback);
  }
}

export function createOnMessageChannel(connection) {
  return eventChannel(emit => {
    const handler = function (msg) {
      emit(msg);
    };
    connection.onMessage(handler);
    return () => {
      connection.removeOnMessage(handler)
    };
  })
}

export function createOnRecipientAckChannel(connection) {
  return eventChannel(emit => {
    const handler = function (id) {
      emit(id);
    };
    connection.onRecipientAck(handler);
    return () => {
      connection.removeOnRecipientAck(handler)
    };
  })
}

export function createOnServerAckChannel(connection) {
  return eventChannel(emit => {
    const handler = function (id) {
      emit(id);
    };
    connection.onServerAck(handler);
    return () => {
      connection.removeOnServerAck(handler)
    };
  })
}
