import * as CONSTANTS from 'constants';

export function timeRemainingfrom24(input) {
  var output = '';
  if (input <= CONSTANTS.HOUR_24_TIME) {
    input /= 1000;
    var noOfHoursLeft = Math.floor(input / 3600);
    var noOfMinutesLeft = Math.floor((input % 3600) / 60);
    if (noOfHoursLeft > 0) {
      output += noOfHoursLeft + ' hrs ';
      if (noOfMinutesLeft !== 0) {
        output += Math.floor(noOfMinutesLeft) + ' minutes left';
      } else {
        output += 'remaining';
      }
    } else {
      if (noOfMinutesLeft === 0) {
        output += '0 minute left';
      } else {
        output += Math.floor(noOfMinutesLeft) + ' minutes left';
      }
    }
    return output;
  }
}

export function timeAgo(input) {
  var output = '';
  var postDate = new Date(input);
  var nowDate = new Date();
  if (nowDate.getYear() - postDate.getYear() === 0) {
    var diff = postDate.getDate() - nowDate.getDate();
    if (diff === 0 || diff === 1) {
      if (diff === 1) {
        output += 'Yesterday';
      }
      if (postDate.getHours() === 12) {
        output += ' ' + postDate.getHours() + ':';
      } else {
        output += ' ' + postDate.getHours() % 12 + ':';
      }
      postDate.getMinutes() < 10 ? output += '0' : output += '';
      output += postDate.getMinutes();
      postDate.getHours() >= 12 ? output += ' PM' : output += ' AM';
    } else {
      output += addDateAndMonth(output, postDate);
    }
  } else {
    output += addDateAndMonth(output, postDate);
    output = output + ' ' + postDate.getFullYear().toString().substring(2, 4);
  }
  return output;
}

function addDateAndMonth(input, postDate) {
  var output = input;
  output += postDate.getDate() + ' ';
  switch (postDate.getMonth()) {
    case 0:
      output += 'Jan';
      break;
    case 1:
      output += 'Feb';
      break;
    case 2:
      output += 'Mar';
      break;
    case 3:
      output += 'Apr';
      break;
    case 4:
      output += 'May';
      break;
    case 5:
      output += 'Jun';
      break;
    case 6:
      output += 'Jul';
      break;
    case 7:
      output += 'Aug';
      break;
    case 8:
      output += 'Sept';
      break;
    case 9:
      output += 'Oct';
      break;
    case 10:
      output += 'Nov';
      break;
    case 11:
      output += 'Dec';
      break;
    default:
  }
  return output;
}
