import 'helpers/checkout';
import PSTCLogo from 'images/pstc-logo.png';
export function openModal(personDetails, paymentDetails) {
  return new Promise((resolve, reject) => {
    var options = {
      'key': RAZORPAY_KEY, // eslint-disable-line no-undef
      'amount': paymentDetails.amount * 100, // 2000 paise = INR 20
      'name': 'PSTakeCare Consult',
      'description': 'Consultation With Doctor',
      'image': PSTCLogo,
      'order_id': paymentDetails.order_id,
      'handler': function (response) {
        if (response.error) {
          reject(response);
        } else {
          resolve(response);
        }
      },
      'prefill': {
        'name': personDetails && personDetails.name,
        'email': personDetails && personDetails.email,
        'contact': personDetails && personDetails.phoneNo
      },
      'theme': {
        'color': '#00A4B0'
      },
      'modal': {
        'ondismiss': function () {
          reject({
            err: 'dismissed modal'
          });
        }
      }
    };
    var razprpay_modal = new Razorpay(options); // eslint-disable-line no-undef
    razprpay_modal.open();
  });
}
