import * as CONSTANTS from 'constants';
export function sendGA(eventCategory, eventAction, eventLabel, value) {
  if (CONSTANTS.WEBSITE_GA_ID) {
    ga('pstcChatTracker.send', {
      hitType: 'event',
      eventCategory,
      eventAction,
      eventLabel,
      value
    });
  }
  ga('otherTracker.send', {
    hitType: 'event',
    eventCategory,
    eventAction,
    eventLabel,
    value
  });
  return true;
}
export function createGAandSendPageView(gaID) {
  if (gaID) {
    ga('create', gaID, 'auto', 'pstcChatTracker');
    sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.LOAD_PLUGIN, CONSTANTS.ALL_VISITORS);
  }
  ga('create', DEFAULT_GA_ID, 'auto', 'otherTracker');
  ga('otherTracker.send', 'pageview');
  sendGA(CONSTANTS.PSTAKECARE_WEBPLUGIN, CONSTANTS.LOAD_PLUGIN, CONSTANTS.ALL_VISITORS);
}
