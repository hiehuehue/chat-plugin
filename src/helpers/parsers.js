export function getDomain() {
  var hostnameArr = window.location.hostname.split('.');
  var domain = (hostnameArr.length === 1) ? hostnameArr[0] : hostnameArr[hostnameArr.length - 2];
  return domain;
}

export function parseChatHistoryToMessagePacket(messages, data) {
  return messages.map((message) => parseHistoryMsgToMessagePacket(message, data));
}

function parseHistoryMsgToMessagePacket(msg, data) {
  const {
    from,
    msg_id,
    to,
    status
  } = msg;
  const {
    body,
    timestamp,
    msg_type,
    query_id
  } = JSON.parse(msg.json);
  const {
    username,
    userJid,
    jidAndNameForSupport: {
      responder: botName,
      jid: botJid
    },
    jidAndNameForDoctor: {
      responder: doctorName,
      jid: doctorJid
    }
  } = data;
  var responder = msg.from === userJid ? username : msg.from === botJid ? botName : msg.from === doctorJid ? doctorName : undefined;
  return {
    body,
    from,
    msg_type,
    msgId: msg_id,
    query_id,
    status,
    to,
    timestamp,
    responder
  }
}

export function getFileNameFromFileUrl(fileurl) {
  var UrlSplitArr = fileurl.split(/\/|\?/);
  return UrlSplitArr[UrlSplitArr.length - 2];
}
