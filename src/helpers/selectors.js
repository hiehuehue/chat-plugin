export const getReceiverJidAndName = (state) => state.receiverJidAndName;
export const getJidAndNameForDoctor = (state) => state.jidAndNameForDoctor;
export const getResendTick = (state) => state.resendTick;
export const getScreenState = (state) => state.screenState;
export const getPaymentDoneTime = (state) => state.paymentDoneTime;
export const getHour24Timer = (state) => state.hour24Timer;
export const getChatPassword = (state) => state.jidAndPassword.chatPassword;
export const getVisibility = (state) => state.visibility;
export const getUserName = (state) => state.name;
export const getUserJid = (state) => state.jidAndPassword.jid;
export const getJidAndNameForSupport = (state) => state.jidAndNameForSupport;
export const getAllUnSentMessages = (state) => {
  return state.allMessages.filter((message) => {
    return message.status === 0
  });
};
export const getAllMessages = (state) => state.allMessages;
export const getTotalFees = (state) => state.queryPaymentDetails.total_fees;
export const getState = (state) => state;
