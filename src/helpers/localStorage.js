export function getItem(itemName) {
  try {
    const serialisedItem = localStorage.getItem(itemName);
    if (serialisedItem === null) {
      return undefined;
    }
    return JSON.parse(serialisedItem);
  } catch (e) {
    return undefined;
  }
}
export function saveItem(itemName, item) {
  try {
    const serialisedItem = JSON.stringify(item);
    localStorage.setItem(itemName, serialisedItem);
    return true;
  } catch (e) {
    return undefined;
  }
}
export function removeItem(itemName) {
  try {
    localStorage.removeItem(itemName);
    return true;
  } catch (e) {
    return undefined;
  }
}
