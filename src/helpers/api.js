import 'whatwg-fetch';
import fetchIntercept from 'fetch-intercept';
import {
  getDomain
} from 'helpers/parsers';

function addParams(url, config) {
  var href = window.location.href;
  var domain = getDomain();
  var newUrl = 'consumer_version=1.0&platform=web_consult&source_url=' + encodeURIComponent(href) + '&domain=' + domain;
  if (url.indexOf('?') === -1) {
    newUrl = url + '?' + newUrl;
  } else {
    newUrl = url + '&' + newUrl;
  }
  return [newUrl, config];
}

fetchIntercept.register({
  request: function (url, config) {
    var newRequest = addParams(url, config);
    return newRequest;
  },

  requestError: function (error) {
    // Called when an error occured during another 'request' interceptor call
    return Promise.reject(error);
  },

  response: function (response) {
    // Modify the reponse object
    return response;
  },

  responseError: function (error) {
    // Handle an fetch error
    return Promise.reject(error);
  }
});

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response
  } else {
    var error = new Error(response.statusText)
    error.response = response
    throw error
  }
}

function parseJSON(response) {
  return response.json()
}
// FIXME:  CORS_API_STAGING

export function getJidAndPassword(token) {
  return fetch(CORS_API_STAGING + '/api/v2/jids', { // eslint-disable-line no-undef
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Token token=' + token
    }
  }).then(checkStatus).then(parseJSON)
}

export function chatHistory(queryId, time, token) {
  return fetch(CORS_API_APPLENEST + '/api/v1/customers/chat_histories?query_id=' + queryId + '&timestamp=' + time, { // eslint-disable-line no-undef
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Token token=' + token
    }
  }).then(checkStatus).then(parseJSON)
}

export function doctorRating(queryId, rating, token) {
  return fetch(CORS_API_APPLENEST + '/api/v1/customers/doctor_ratings', { // eslint-disable-line no-undef
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Token token=' + token
    },
    body: JSON.stringify({
      'query_id': queryId,
      'doctor_ratings': rating
    })
  }).then(checkStatus).then(parseJSON)
}

export function botRating(queryId, rating, token) {
  return fetch(CORS_API_APPLENEST + '/api/v1/customers/bot_ratings', { // eslint-disable-line no-undef
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Token token=' + token
    },
    body: JSON.stringify({
      'query_id': queryId,
      'bot_rating': rating
    })
  }).then(checkStatus).then(parseJSON)
}

export function makeZeroPayment(query_id, token, promoCodeDetails) {
  return fetch(CORS_API_APPLENEST + '/api/v1/customers/payments/zero_payment', { // eslint-disable-line no-undef
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Token token=' + token
    },
    body: JSON.stringify({
      'query_id': query_id,
      'promo_id': promoCodeDetails && promoCodeDetails.id
    })
  }).then(checkStatus).then(parseJSON)
}

export function getQueryPaymentDetails(query_id, token) {
  return fetch(CORS_API_APPLENEST + '/api/v1/customers/payments?query_id=' + query_id, { // eslint-disable-line no-undef
    method: 'GET',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Token token=' + token
    }
  }).then(checkStatus).then(parseJSON)
}

export function getPromocodeDetails(query_id, code, token) {
  return fetch(CORS_API_APPLENEST + '/api/v1/customers/promocodes/apply', { // eslint-disable-line no-undef
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Token token=' + token
    },
    body: JSON.stringify({
      'query_id': query_id,
      'code': code
    })
  }).then(checkStatus).then(parseJSON)
}

export function getPaymentDetails(queryId, token, promoCodeDetails) {
  return fetch(CORS_API_APPLENEST + '/api/v1/customers/payments/initiate_payment', { // eslint-disable-line no-undef
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Token token=' + token
    },
    body: JSON.stringify({
      'query_id': queryId,
      'promo_id': promoCodeDetails && promoCodeDetails.id
    })
  }).then(checkStatus).then(parseJSON)
}

export function completePayment(queryId, token) {
  return fetch(CORS_API_APPLENEST + '/api/v1/customers/payments/complete_payment', { // eslint-disable-line no-undef
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Token token=' + token
    },
    body: JSON.stringify({
      'query_id': queryId
    })
  }).then(checkStatus).then(parseJSON)
}

export function showDoctorProfile(id, token) {
  return fetch(CORS_API_APPLENEST + '/api/v1/customers/doctors/' + id, { // eslint-disable-line no-undef
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Token token=' + token
    }
  }).then(checkStatus).then(parseJSON)
}

export function mobileVerificationStatus(number) {
  return fetch(CORS_API_STAGING + '/api/v2/phone/accounts/verification_status', { //eslint-disable-line no-undef
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      'phone': number
    })
  }).then(checkStatus).then(parseJSON)
}

export function phoneRegistration(name, phone) {
  return fetch(CORS_API_STAGING + '/api/v2/phone/registrations', { //eslint-disable-line no-undef
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      'name': name,
      'phone': phone,
      'signup_utility': 'signup_form',
      'consumer': 'web'
    })
  }).then(checkStatus).then(parseJSON)
}

export function sendOTPForVerifiedUser(number) {
  return fetch(CORS_API_STAGING + '/api/v2/phone/otp', { //eslint-disable-line no-undef
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      phone: number.toString()
    })
  }).then(checkStatus).then(parseJSON)
}

export function sendOTPForNonVerifiedUser(number, token) {
  return fetch(CORS_API_STAGING + '/api/v2/confirmations/resend_phone', { //eslint-disable-line no-undef
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      phone: number.toString(),
      authentication_token: token
    })
  }).then(checkStatus).then(parseJSON)
}

export function confirmOTPForNonVerifiedUser(number, otp, token) {
  return fetch(CORS_API_STAGING + '/api/v2/confirmations/confirm_phone', { //eslint-disable-line no-undef
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      'authentication_token': token,
      'code': otp,
      'phone': number
    })
  }).then(checkStatus).then(parseJSON)
}

export function confirmOTPForVerifiedUser(number, otp) {
  return fetch(CORS_API_STAGING + '/api/v2/phone/sessions', { //eslint-disable-line no-undef
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      'code': otp,
      'phone': number
    })
  }).then(checkStatus).then(parseJSON).catch((err) => {
    console.log(err);
    throw err;
  });
}

export function querySubmission(jid, name, body, phone, token) {
  return fetch(CORS_API_APPLENEST + '/api/v1/queries', { // eslint-disable-line no-undef
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Token token=' + token
    },
    body: JSON.stringify({
      customer: {
        jid,
        name,
        phone
      },
      query: {
        body,
        additional_info: {
          allergies: '',
          known_conditions: '',
          medications: ''
        },
        patient_details: {
          name,
          relation: '',
          sex: '',
          dob: '',
          height: '',
          weight: ''
        }
      }
    })
  }).then(checkStatus).then(parseJSON)
}

export function postLeadQueries(body) {
  return fetch(CORS_API_STAGING + '/api/v1/lead_queries', { //eslint-disable-line no-undef
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  }).then(checkStatus).then(parseJSON)
}

export function putLeadQueries(body, id) {
  return fetch(CORS_API_STAGING + '/api/v1/lead_queries/' + id, { //eslint-disable-line no-undef
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  }).then(checkStatus).then(parseJSON)
}
export function uploadFile(file) {
  var fd = new FormData(); // will not work in IE8, well nothing will work on IE8
  fd.append('file', file);
  return fetch(CORS_API_APPLENEST + '/api/v1/attachments', { // eslint-disable-line no-undef
    method: 'POST',
    body: fd
  }).then(checkStatus).then(parseJSON)
}
