import 'regenerator-runtime/runtime';
import React from 'react';
import {render, unmountComponentAtNode} from 'react-dom';
import MainWrapper from './wrappers/MainWrapper';
import {Provider} from 'react-redux';
import injectTapEventPlugin from 'react-tap-event-plugin';
import {createStore, compose, applyMiddleware} from 'redux';
import rootReducer from './reducers';
import adapter from 'redux-localstorage/lib/adapters/localStorage';
import {getItem, saveItem} from 'helpers/localStorage';
import persistState, {mergePersistedState} from 'redux-localstorage';
import createSagaMiddleware from 'redux-saga';
import rootSaga from 'sagas/saga';
import 'helpers/analytics';
import ResponsiveComponent from 'components/ResponsiveComponent';
import {changeWebsiteGaId} from 'constants';
injectTapEventPlugin();
const sagaMiddleware = createSagaMiddleware();

const reducer = compose(mergePersistedState())(rootReducer);

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
if(!getItem('PSTC-Chat-App-v1')){
  saveItem('PSTC-Chat-App-v1',createStore(reducer).getState());
}
const initialState = getItem('PSTC-Chat-App-v1');
const enhancer = composeEnhancer(applyMiddleware(sagaMiddleware), persistState(adapter(window.localStorage), 'PSTC-Chat-App-v1'));
const store = createStore(reducer, initialState, enhancer);
let sagaTask = sagaMiddleware.run(function* () {
  yield rootSaga()
});

if (module.hot) {
  module.hot.accept('./reducers/', () => {
    const nextRootReducer = require('./reducers/index').default;
    store.replaceReducer(nextRootReducer);
  });
  module.hot.accept('./sagas/saga', () => {
    //will work but chrome source maps will not be instantly updated
    //your only friend is ALT+R https://github.com/webpack/webpack/issues/2478
    const getNewSagas = require('./sagas/saga').default;
    sagaTask.cancel()
    sagaTask.done.then(() => {
      sagaTask = sagaMiddleware.run(function* replacedSaga() {
        yield getNewSagas()
      })
    })
  })
}

window.PSTCChat = (function () {
  var div, initiated = false, instance;
  if(instance){
    return instance;
  } else {
    function changeDivStyling(div, options) {
      options = options || {};
      div.style['z-index'] = options['z-index'] || 654321;
      div.style.right = options.right || '16px';
      changeWebsiteGaId(options.gaId);
      return div;
    }
    function init(options) {
      options = options || {};
      if(!initiated){
        div = document.createElement('div');
        div.id = 'PSTC-Chat-App';
        div.style.position = 'fixed';
        div.style.bottom = '0px';
        div.style['font-family'] = '"Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif';
        div = changeDivStyling(div, options);
        document.body.appendChild(div);
        render(
          <Provider store={store}>
            <ResponsiveComponent>
              <MainWrapper/>
            </ResponsiveComponent>
          </Provider>, document.getElementById('PSTC-Chat-App'));
          initiated =  true;
        return function changeDivStyling(options) {
          changeDivStyling(div, options);
        };
      }else {
        return false;
      }
    }
    function close() {
      try {
        unmountComponentAtNode(div)
        document.body.removeChild(div);
        initiated = false;
        return true;
      } catch (e) {
        console.log(e);
        return false;
      }
    }
    function createInstance() {
      return {
        init: init,
        close: close
      }
    }
    instance = createInstance();
    return instance;
  }
})()
window.PSTCChat.init();
