import * as CONSTANTS from 'constants';

export function changeLoadingScreenVisibility(state = false, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_LOADING_SCREEN_VISIBILITY:
      return action.bool;
    case CONSTANTS.OPEN_NEW_QUERY:
      return false;
    default:
      return state;
  }
}
export function changePaymentSubmitAbility(state = false, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_PAYMENT_SUBMIT_ABILITY:
      return action.bool;
    case CONSTANTS.OPEN_NEW_QUERY:
      return false;
    default:
      return state;
  }
}

export function changeTitle(state = CONSTANTS.INITIAL_TITLE, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_TITLE:
      return action.title;
    case CONSTANTS.OPEN_NEW_QUERY:
      return CONSTANTS.INITIAL_TITLE;
    default:
      return state;
  }
}

export function changeUnreadMessageCount(state = 0, action) {
  switch (action.type) {
    case CONSTANTS.INCREASE_UNREAD_MESSAGE_COUNT:
      return state + 1;
    case CONSTANTS.CLEAR_UNREAD_MESSAGE_COUNT:
    case CONSTANTS.OPEN_NEW_QUERY:
      return 0;
    default:
      return state;
  }
}

export function changeBottomActionButton(state = CONSTANTS.FIRST_SCREEN_NEXT, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_BOTTOM_ACTION_BUTTON:
      return action.value;
    case CONSTANTS.CHANGE_TO_PAYMENT_SCREEN:
      return CONSTANTS.CONTINUE_TO_PAYMENT;
    case CONSTANTS.CHANGE_NUMBER:
      return CONSTANTS.FIRST_SCREEN_NEXT;
    case CONSTANTS.OPEN_NEW_QUERY:
      return CONSTANTS.SUMBIT_NEW_QUERY;
    default:
      return state;
  }
}

export function screenState(state = CONSTANTS.GET_QUERY_PHONE_NAME_SCREEN, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_SCREEN:
      return action.screenState;
    case CONSTANTS.CHANGE_NUMBER:
      return CONSTANTS.GET_QUERY_PHONE_NAME_SCREEN;
    case CONSTANTS.OPEN_NEW_QUERY:
      return CONSTANTS.NEW_QUERY_SCREEN;
    default:
      return state;
  }
}

export function popUpScreen(state = '', action) {
  switch (action.type) {
    case CONSTANTS.POP_UP_SCREEN:
      return action.value;
    case CONSTANTS.CHANGE_TO_PAYMENT_SCREEN:
      return CONSTANTS.PAYMENT_SCREEN;
    case CONSTANTS.OPEN_NEW_QUERY:
      return '';
    default:
      return state;
  }
}

export function errorMessages(state = '', action) {
  switch (action.type) {
    case CONSTANTS.PUSH_ERROR:
      return action.value;
    case CONSTANTS.CLOSE_ERROR_SCREEN:
      return '';
    default:
      return state
  }
}
