import * as CONSTANTS from 'constants';

export function changePaymentSubmitAbility(state = true, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_PAYMENT_SUBMIT_ABILITY:
      return action.bool;
    case CONSTANTS.OPEN_NEW_QUERY:
      return true;
    default:
      return state;
  }
}

export function changePaymentCompleteStatus(state = false, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_PAYMENT_COMPLETE_STATUS:
      return action.bool;
    case CONSTANTS.OPEN_NEW_QUERY:
      return false;
    default:
      return state;
  }
}

export function savePaymentDetails(state = {}, action) {
  switch (action.type) {
    case CONSTANTS.SAVE_PAYMENT_DETAILS:
      return action.value;
    case CONSTANTS.OPEN_NEW_QUERY:
      return {};
    default:
      return state
  }
}

export function changePromocode(state = '', action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_PROMOCODE:
      return action.value;
    case CONSTANTS.OPEN_NEW_QUERY:
      return '';
    default:
      return state;
  }
}

export function savePromocodeDetails(state = {}, action) {
  switch (action.type) {
    case CONSTANTS.SAVE_PROMOCODE_DETAILS:
      return action.value;
    case CONSTANTS.OPEN_NEW_QUERY:
      return {};
    default:
      return state;
  }
}

export function saveQueryPaymentDetails(state = {}, action) {
  switch (action.type) {
    case CONSTANTS.SAVE_QUERY_PAYMENT_DETAILS:
      return action.value;
    case CONSTANTS.OPEN_NEW_QUERY:
      return {}
    default:
      return state;
  }
}
