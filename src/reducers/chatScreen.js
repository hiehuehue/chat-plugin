import * as CONSTANTS from 'constants';

export function storeJidAndNameForSupport(state = {}, action) {
  switch (action.type) {
    case CONSTANTS.STORE_JID_AND_NAME_FOR_SUPPORT:
      return action.value;
    case CONSTANTS.OPEN_NEW_QUERY:
      return {};
    default:
      return state;
  }
}

export function change24HourTimerVisibility(state = false, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_24_HOUR_TIMER_VISIBILITY:
      return action.bool;
    case CONSTANTS.OPEN_NEW_QUERY:
      return false;
    default:
      return state;
  }
}

export function savePaymentDoneTime(state = '', action) {
  switch (action.type) {
    case CONSTANTS.SAVE_PAYMENT_DONE_TIME:
      return action.time;
    case CONSTANTS.OPEN_NEW_QUERY:
      return ''
    default:
      return state;
  }
}

export function change24hourTime(state = CONSTANTS.HOUR_24_TIME, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_24_HOUR_TIME:
      return action.time;
    case CONSTANTS.OPEN_NEW_QUERY:
      return CONSTANTS.HOUR_24_TIME;
    default:
      return state;
  }
}

export function changeRating(state = 0, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_RATING:
      return action.rating;
    case CONSTANTS.OPEN_NEW_QUERY:
      return 0;
    default:
      return state;
  }
}

export function storeMessages(state = [], action) {
  switch (action.type) {
    case CONSTANTS.STORE_MESSAGES:
      // will be handled server side or will it be?
      // var oldMsgsWithSameId = state.filter((item) => {
      //   return item.msgId === action.msg.msgId
      // });
      // if (oldMsgsWithSameId.length > 0) {
      //   return state;
      // } else {
      // }
      return [...state, action.msg];
    case CONSTANTS.DO_DOUBLE_TICK:
      return state.map((msg) => {
        if (msg.msgId === action.id) {
          return Object.assign({}, msg, {
            status: 2
          });
        }
        return msg;
      });
    case CONSTANTS.DO_SINGLE_TICK:
      return state.map((msg) => {
        if (msg.msgId === action.id) {
          return Object.assign({}, msg, {
            status: 1
          });
        }
        return msg;
      });
    case CONSTANTS.AMMEND_STORED_MESSAGE:
      return state.map((msg) => {
        if (msg[action.key] === action.old) {
          return Object.assign({}, msg, {
            [action.key]: action.new
          });
        }
        return msg;
      });
    case CONSTANTS.OPEN_NEW_QUERY:
      return []
    default:
      return state
  }
}

export function changeMessageValue(state = '', action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_MESSAGE_VALUE:
      return action.value
    case CONSTANTS.OPEN_NEW_QUERY:
      return '';
    default:
      return state
  }
}

export function storeJidAndNameForDoctor(state = {}, action) {
  switch (action.type) {
    case CONSTANTS.STORE_JID_AND_NAME_FOR_DOCTOR:
      return action.value;
    case CONSTANTS.OPEN_NEW_QUERY:
      return {};
    default:
      return state
  }
}

export function changeReceiverJidAndName(state = {
  jid: CONSTANTS.SUPPORT_BOT_JID,
  responder: CONSTANTS.SUPPORT_BOT_NAME
}, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_RECEIVER_JID_AND_NAME:
      return action.value
    case CONSTANTS.OPEN_NEW_QUERY:
      return {
        jid: CONSTANTS.SUPPORT_BOT_JID,
        responder: CONSTANTS.SUPPORT_BOT_NAME
      }
    default:
      return state;
  }
}
