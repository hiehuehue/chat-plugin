import * as CONSTANTS from 'constants';

export function saveDoctorProfileId(state = '', action) {
  switch (action.type) {
    case CONSTANTS.SAVE_DOCTOR_PROFILE_ID:
      return action.id;
    case CONSTANTS.OPEN_NEW_QUERY:
      return '';
    default:
      return state;
  }
}

export function saveDoctorProfile(state = {}, action) {
  switch (action.type) {
    case CONSTANTS.SAVE_DOCTOR_PROFILE:
      return action.value;
    case CONSTANTS.OPEN_NEW_QUERY:
      return {};
    default:
      return state
  }
}
