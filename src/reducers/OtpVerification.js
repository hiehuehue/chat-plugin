import * as CONSTANTS from 'constants';

export function changeOtpErrorMessage(state = '', action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_OTP_ERROR_MESSAGE:
      return action.error;
    case CONSTANTS.OPEN_NEW_QUERY:
      return '';
    default:
      return state;
  }
}

export function changeResendButtonVisibility(state = false, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_RESEND_BUTTON_VISIBILITY:
      return action.bool;
    case CONSTANTS.OPEN_NEW_QUERY:
      return false;
    default:
      return state;
  }
}

export function changeResendTick(state = CONSTANTS.RESEND_TIME, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_RESEND_TICK:
      return action.tick;
    case CONSTANTS.OPEN_NEW_QUERY:
      return CONSTANTS.RESEND_TIME;
    default:
      return state;
  }
}

export function changeQueryId(state = '', action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_QUERY_ID:
      return action.id
    case CONSTANTS.OPEN_NEW_QUERY:
      return '';
    default:
      return state
  }
}
export function saveJidAndPassword(state = {}, action) {
  switch (action.type) {
    case CONSTANTS.SAVE_JID_AND_PASSWORD:
      return action.value
    default:
      return state
  }
}

export function changeOTPValue(state = '', action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_OTP_VALUE:
      return action.value;
    case CONSTANTS.OPEN_NEW_QUERY:
      return '';
    default:
      return state;
  }
}

export function otpSubmitAbility(state = true, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_OTP_SUBMIT_ABILITY:
      return action.bool;
    case CONSTANTS.OPEN_NEW_QUERY:
      return true;
    default:
      return state
  }
}

export function changeOtpResendAbility(state = true, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_OTP_RESEND_ABILITY:
      return action.bool;
    case CONSTANTS.OPEN_NEW_QUERY:
      return true;
    default:
      return state
  }
}
