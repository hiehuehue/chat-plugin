import * as CONSTANTS from 'constants';

export function changeToInitiated(state = true, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_TO_INITIATED:
      return false;
    default:
      return state;
  }
}

export default function visibility(state = false, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_VISIBILITY_OF_CHAT:
      return !state;
    default:
      return state;
  }
}
export function changeFirstUser(state = true, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_FIRST_USER:
      return !state;
    default:
      return state;
  }
}
