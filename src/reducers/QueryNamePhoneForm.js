import * as CONSTANTS from 'constants';

export function saveUserEmail(state = '', action) {
  switch (action.type) {
    case CONSTANTS.SAVE_USER_EMAIL:
      return action.email;
    default:
      return state;
  }
}

export function changeErrorForNewQueryBody(state = '', action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_ERROR_FOR_NEW_QUERY_BODY:
      return action.error;
    case CONSTANTS.OPEN_NEW_QUERY:
      return '';
    default:
      return state;
  }
}

export function changeQueryReSubmitAbility(state = false, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_QUERY_RE_SUBMIT_ABILITY:
      return action.bool;
    case CONSTANTS.OPEN_NEW_QUERY:
      return false;
    default:
      return state;
  }
}

export function changeErrorMessageForPhoneNo(state = '', action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_ERROR_MESSAGE_FOR_PHONE_NO:
      return action.error;
    default:
      return state;
  }
}

export function changeErrorMessageForName(state = '', action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_ERROR_MESSAGE_FOR_NAME:
      return action.error;
    default:
      return state;
  }
}

export function saveAuthenticationToken(state = '', action) {
  switch (action.type) {
    case CONSTANTS.SAVE_AUTHENTICATION_TOKEN:
      return action.value;
    default:
      return state;
  }
}


export function userVerificationStatus(state = 'false', action) {
  switch (action.type) {
    case CONSTANTS.SET_USER_VERIFICATION_STATUS:
      return action.bool;
    default:
      return state;
  }
}

export function changeName(state = '', action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_NAME:
      return action.value;
    default:
      return state;
  }
}

export function changePhoneNo(state = '', action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_PHONENO:
      return action.value;
    default:
      return state;
  }
}

export function changeQueryBody(state = '', action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_QUERY_BODY:
      return action.value;
    case CONSTANTS.OPEN_NEW_QUERY:
      return '';
    default:
      return state;
  }
}

export function changeSubmitAbility(state = true, action) {
  switch (action.type) {
    case CONSTANTS.CHANGE_SUBMIT_ABILITY:
      return action.bool;
    case CONSTANTS.OPEN_NEW_QUERY:
      return true;
    default:
      return state
  }
}
