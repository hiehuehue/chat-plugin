import {
  combineReducers
} from 'redux';

import visibility, {
  changeFirstUser as isFirstUser,
  changeToInitiated as isUnInitiated
} from './visibility';
import {
  screenState,
  popUpScreen as popUpScreenName,
  errorMessages as errors,
  changeBottomActionButton as bottomActionButton,
  changeUnreadMessageCount as unreadMessageCount,
  changeTitle as title,
  changeLoadingScreenVisibility as loadingScreenVisibility
} from './screenState';
import {
  changeOTPValue as otp,
  otpSubmitAbility as otpSubmitAbility,
  changeOtpResendAbility as otpResendAbility,
  changeResendButtonVisibility as resendButtonVisibility,
  saveJidAndPassword as jidAndPassword,
  changeQueryId as queryId,
  changeResendTick as resendTick,
  changeOtpErrorMessage as otpErrorMessage
} from './OtpVerification';
import {
  storeMessages as allMessages,
  changeMessageValue as message,
  changeReceiverJidAndName as receiverJidAndName,
  storeJidAndNameForDoctor as jidAndNameForDoctor,
  changeRating as rating,
  savePaymentDoneTime as paymentDoneTime,
  change24hourTime as hour24Timer,
  change24HourTimerVisibility as hour24TimerVisibility,
  storeJidAndNameForSupport as jidAndNameForSupport
} from './chatScreen';
import {
  changeName as name,
  changePhoneNo as phoneNo,
  changeQueryBody as queryBody,
  changeSubmitAbility as submitAbility,
  changeQueryReSubmitAbility as queryReSubmitAbility,
  userVerificationStatus as userVerificationStatus,
  saveAuthenticationToken as authenticationToken,
  changeErrorMessageForPhoneNo as errorForPhoneNo,
  changeErrorMessageForName as errorForName,
  changeErrorForNewQueryBody as errorForNewQueryBody,
  saveUserEmail as email
} from './QueryNamePhoneForm';
import {
  saveDoctorProfile as doctorProfile,
  saveDoctorProfileId as doctorId
} from './DoctorProfile';

import {
  savePaymentDetails as paymentDetails,
  changePromocode as promoCode,
  savePromocodeDetails as promoCodeDetails,
  saveQueryPaymentDetails as queryPaymentDetails,
  changePaymentCompleteStatus as paymentCompleteStatus,
  changePaymentSubmitAbility as paymentSubmitAbility
} from './PaymentDetails';

export default combineReducers({
  isFirstUser,
  isUnInitiated,
  visibility,
  screenState,
  name,
  email,
  title,
  phoneNo,
  queryBody,
  errorForName,
  errorForPhoneNo,
  submitAbility,
  otp,
  otpErrorMessage,
  errorForNewQueryBody,
  userVerificationStatus,
  unreadMessageCount,
  authenticationToken,
  paymentCompleteStatus,
  otpSubmitAbility,
  otpResendAbility,
  queryReSubmitAbility,
  resendButtonVisibility,
  hour24Timer,
  jidAndPassword,
  allMessages,
  message,
  receiverJidAndName,
  bottomActionButton,
  loadingScreenVisibility,
  queryId,
  doctorProfile,
  resendTick,
  paymentDetails,
  paymentSubmitAbility,
  popUpScreenName,
  rating,
  doctorId,
  paymentDoneTime,
  jidAndNameForDoctor,
  jidAndNameForSupport,
  hour24TimerVisibility,
  errors,
  promoCode,
  promoCodeDetails,
  queryPaymentDetails
});
